<?php

namespace RESTAPI;

/**
 * Exception class dedicated to REST API Application
 *
 * @author Simon Yang <syang@ssssoft.com>
 *
 */
class RESTApiException extends \Exception {

	/**
	 * Convenience function to generate exception representation for Slim to use.
	 * Eg. $app->halt($error->toSlim());
	 *
	 * @return multitype:string NULL
	 */
	public function toSlim() {
		return array($this->getCode(), "{'error': '{$this->getMessage()}'}");
	}

}