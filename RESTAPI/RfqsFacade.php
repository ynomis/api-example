<?php

/**
 * Assume Database class is loaded in global name space. If it's not, it should
 * be 'required' here, /inc/db/pdo.php
 *
 * @author Simon Yang <syang@ssssoft.com>
 *
 */

namespace RESTAPI;

/**
 * Facade class for order related actions
 *
 */
class RfqsFacade {

	/**
	 * Get tracking number list for a ma company by given date range.
	 * If client_company_id is provided, filter result on that client company
	 *
	 * @param int $macompanyid
	 * @param \Slim\Http\Request $request
	 * @throws RESTApiException
	 * @return array
	 */
	public function getRfqIdsByDates($macompanyid, $request) {
		$where_clause = '';
	
		$startDate = $request->params('startDate');
		$endDate   = $request->params('endDate');
		$clientCompanyId = $request->params('customerId');

		if ($startDate && $endDate) {
			$where_clause .= "AND rfq.initiated BETWEEN '$startDate'::TIMESTAMP AND '$endDate'::TIMESTAMP ";
		} else {
			throw new RESTApiException("startDate and/or endDate is missing.", 400);
		}

		$join_clause = '';
		if ($clientCompanyId) {
			$join_clause  = "INNER JOIN clients c ON rfq.client_id = c.id
							 INNER JOIN client_companies cc ON c.client_company_id = cc.id ";
			$where_clause .= "AND cc.id = ? ";
		}

		$sSql = <<<RFQID_SQL
SELECT
	rfq.tracking_number
FROM rfq
	{$join_clause}
WHERE
	rfq.ma_company_id = ?
	{$where_clause}
ORDER BY rfq.tracking_number
RFQID_SQL;

		try {
			$dbh = \Database::get_connection();
			$stmt = $dbh->prepare($sSql);
			$stmt->bindValue(1, $macompanyid,	\PDO::PARAM_INT);
			if ($clientCompanyId)
				$stmt->bindValue(2, $clientCompanyId, \PDO::PARAM_INT);
			$stmt->execute();
			return $stmt->fetchAll(\PDO::FETCH_COLUMN, 0);
		} catch (\PDOException $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Internal errors.', 503);
		}

	}

	/**
	 * Get a quote id list for a ma company.
	 * If client company id is provided, filter result on that client company.
	 *
	 * @param int $macompanyid
	 * @param \Slim\Http\Request $request
	 * @throws RESTApiException
	 * @return array
	 */
	public function getQuoteIdsByDates($macompanyid, $request) {
		$where_clause = '';
		
		$startDate = $request->params('startDate');
		$endDate   = $request->params('endDate');
		$clientCompanyId = $request->params('customerId');
		
		if ($startDate && $endDate) {
			$where_clause .= "AND rfq.initiated BETWEEN '$startDate'::TIMESTAMP AND '$endDate'::TIMESTAMP ";
		} else {
			throw new RESTApiException("startDate and/or endDate is missing.", 400);
		}
		
		$join_clause = '';
		if ($clientCompanyId) {
			$join_clause  = "INNER JOIN clients c ON rfq.client_id = c.id
							 INNER JOIN client_companies cc ON c.client_company_id = cc.id ";
			$where_clause .= "AND cc.id = ? ";
		}
		
		$sSql = <<<QUOTEID_SQL
SELECT
	qs.id AS quote_sq_id, qspl.id AS qlocid, qspl.show
FROM
	quote_sq_per_loc qspl
	INNER JOIN quote_sq qs ON qspl.quote_sq_id = qs.id
	INNER JOIN rfq_idx ri ON qs.rfq_idx_id = ri.id
	INNER JOIN rfq ON ri.rfq_id = rfq.id
	{$join_clause}
WHERE
	rfq.ma_company_id = ?
	{$where_clause}
ORDER BY
	quote_sq_id, qlocid
QUOTEID_SQL;
		
		try {
			$dbh = \Database::get_connection();
			$stmt = $dbh->prepare($sSql);
			$stmt->bindValue(1, $macompanyid,	\PDO::PARAM_INT);
			if ($clientCompanyId)
				$stmt->bindValue(2, $clientCompanyId, \PDO::PARAM_INT);
			$stmt->execute();
			/* Group values by the first column */
			return $stmt->fetchAll(\PDO::FETCH_GROUP | \PDO::FETCH_ASSOC);
		} catch (\PDOException $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Internal errors.', 503);
		}

	}

	public function getQuotebyLocId($macompanyid, $quotelocid) {
		$sql = self::$locationQuoteSQLTemplate;
		$where_clause = <<<QUOTEBYLOCID_SQL
	WHERE qspl.show = 't' AND qspl.id = ? AND rfq.ma_company_id = ?
QUOTEBYLOCID_SQL;
		$sql = str_replace('----LOCATION_QUOTE_SQL_WHERE_CLAUSE----', $where_clause, $sql);

		try {
			$dbh = \Database::get_connection();
			$stmt = $dbh->prepare($sql);
			$stmt->bindValue(1, $quotelocid,	\PDO::PARAM_INT);
			$stmt->bindValue(2, $macompanyid,	\PDO::PARAM_INT);
			$stmt->execute();
			return $stmt->fetch(\PDO::FETCH_ASSOC);
		} catch (\PDOException $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Internal errors.', 503);
		}

	}
	
	public function getQuotebyId($macompanyid, $quoteid) {
		$quoteSql = <<<QUOTEBYID_SQL
SELECT qs.* FROM quote_sq qs
	INNER JOIN rfq_idx ri ON qs.rfq_idx_id = ri.id
	INNER JOIN rfq ON ri.rfq_id = rfq.id
	WHERE qs.id = ? AND rfq.ma_company_id = ?
QUOTEBYID_SQL;

		$sql = self::$locationQuoteSQLTemplate;
		$where_clause = <<<WHERE_CLAUSE
	WHERE qspl.show = 't' AND qs.id = ? AND rfq.ma_company_id = ?
WHERE_CLAUSE;
		$sql = str_replace('----LOCATION_QUOTE_SQL_WHERE_CLAUSE----', $where_clause, $sql);

		try {
			$dbh = \Database::get_connection();
			$stmt = $dbh->prepare($quoteSql);
			$stmt->bindValue(1, $quoteid,		\PDO::PARAM_INT);
			$stmt->bindValue(2, $macompanyid,	\PDO::PARAM_INT);
			$stmt->execute();
			$quote = $stmt->fetch(\PDO::FETCH_ASSOC);

			if (!$quote) {
				throw new RESTApiException("Can not find quote by id: [{$quoteid}]", 400);
			}
			
			$returnValue = array();
			foreach (self::$quoteDataTypes as $field => $properties) {
				if ($properties['read']) {
					$returnValue[$properties['alias']] = $quote[$field];
				}
			}

			$stmt = $dbh->prepare($sql);
			$stmt->bindValue(1, $quoteid,		\PDO::PARAM_INT);
			$stmt->bindValue(2, $macompanyid,	\PDO::PARAM_INT);
			$stmt->execute();
			$returnValue['Locations'] = $stmt->fetchAll(\PDO::FETCH_ASSOC);
			return $returnValue;
		} catch (\PDOException $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Internal errors.', 503);
		}
	
	}

	public function getQuoteSkusbyLocId($macompanyid, $quotelocid) {
		try {
			$loc_quote = \MSS\Quote\LocationQuote::find($quotelocid);
			$rfq = $loc_quote->rfq;
			if ($macompanyid != $rfq->ma_company_id) {
				throw new \ActiveRecord\RecordNotFound('Not in MA company.');
			}
		} catch (\ActiveRecord\RecordNotFound $e) {
			throw new RESTApiException("Can't find location quote by id: [{$quotelocid}]", 400);
		} catch (\PDOException $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Internal errors.', 500);
		}

		$returnValue = array();
		foreach(self::$locQuoteDataTypes as $field => $prop) {
			if ($prop['read']) {
				$_val = $loc_quote->{$field};
				$returnValue[$prop['alias']] =
					($_val instanceof \ActiveRecord\DateTime) ?
						$_val->format(\ActiveRecord\DateTime::$FORMATS['iso8601']) : $_val;
			}
		}
		$loc_req = $loc_quote->loc_requirement; $_locinfo = array();
		foreach(self::$locRequirementDataTypesSKU as $field => $prop) {
			if ($prop['read']) $_locinfo[$prop['alias']] = $loc_req->{$field};
		}
		$returnValue['LocationInfo'] = $_locinfo;

		$skus = $loc_quote->skus; $_skus = array();
		foreach($skus as $sku) {
			$_sku = array();
			foreach(self::$locQuoteSkuDataTypes as $field => $prop) {
				if ($prop['read']) $_sku[$prop['alias']] = $sku->{$field};
			}
			$_skus[] = $_sku;
		}
		$returnValue['SKUs'] = $_skus;

		return $returnValue;
	}

	public function getLocationRequirementById($macompanyid, $reqsqlocid) {
		try {
		 $loc_req = \MSS\Requirement\LocationRequirement::find($reqsqlocid);
		} catch (\ActiveRecord\RecordNotFound $e) {
			throw new RESTApiException("Can't find location requirment by id: [{$reqsqlocid}].", 400);
		} catch (\PDOException $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Internal errors.', 500);
		}

		if ($loc_req->rfq->ma_company_id != $macompanyid) {
			throw new RESTApiException("Can't find location requirment by id: [{$reqsqlocid}].", 400);
		}

		$custom_fields =
			\MSS\Requirement\CustomField::find_all_by_ma_company_id_and_table_name($macompanyid, 'req_sq_idx');

		$returnValue = array();
		foreach (self::$locRequirementDataTypes as $field => $prop) {
			if ($prop['read']) $returnValue[$prop['alias']] = $loc_req->{$field};
		}

		$_normal  = array();
		$_dynamic = array();
		foreach($custom_fields as $_f) {
			$_index = $_f->input_id;
			$_field_value = 'input_value_' . $_index;
			$_field_name = 'input_name_' . $_index;
			if ('[dynamic]' == $_f->short_label) {
				if ($loc_req->{$_field_name}) {
					$_temp = explode('::', $loc_req->{$_field_name});
					$_dynamic[$_index] = array(
						'label' => $_temp[1],
						'value' => $loc_req->{$_field_value}
					);
				}
			} else {
				$_normal[$_index] = array(
					'label' => $_f->label,
					'short' => $_f->short_label,
					'value' => $loc_req->{$_field_value}
				);
			}
		}
		$returnValue['CustomFields'] = $_normal;
		if (!empty($_dynamic)) {
			$returnValue['DynamicFields'] = $_dynamic;
		}

		return $returnValue;
	}

	public function getRequirementById($macompanyid, $reqsqid) {
		try {
			$req = \MSS\Requirement::find($reqsqid);
		} catch (\ActiveRecord\RecordNotFound $e) {
			throw new RESTApiException("Can't find requirment by id: [{$reqsqid}].", 400);
		} catch (\PDOException $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Internal errors.', 500);
		}

		if ($req->rfq->ma_company_id != $macompanyid) {
			throw new RESTApiException("Can't find requirment by id: [{$reqsqid}].", 400);
		}

		$custom_fields =
			\MSS\Requirement\CustomField::find_all_by_ma_company_id_and_table_name($macompanyid, 'req_sq');

		$returnValue = array();
		foreach (self::$requirementDataTypes as $field => $prop) {
			if ($prop['read']) {
				$_val = $req->{$field};
				if (0 === strpos($field, 'provider_id')) {
					if (0 === $_val) {
						$returnValue[$prop['alias']] = 'No Preference';
					} elseif (999999 === $_val) {
						$returnValue[$prop['alias']] = 'None';
					} elseif(isset($_val) && $_val > 0) {
						try {
							$_prvd = \MSS\QuoteProvider::find($_val);
							$returnValue[$prop['alias']] = $_prvd->network;
						} catch (\ActiveRecord\RecordNotFound $e) {
							$returnValue[$prop['alias']] = null;
						}
					} else {
						$returnValue[$prop['alias']] = null;
					}
				} else {
					$returnValue[$prop['alias']] =
						($_val instanceof \ActiveRecord\DateTime) ?
						$_val->format(\ActiveRecord\DateTime::$FORMATS['iso8601']) : $_val;
				}
			}
		}

		$locreqs = $req->loc_requirements;
		$_locrs = array();
		foreach($locreqs as $_r) {
			$_locr = array();
			foreach (self::$locRequirementDataTypesSKU as $field => $prop) {
				if ('req_sq_id' === $field) continue;
				if ($prop['read']) {
					$_locr[$prop['alias']] = $_r->{$field};
				}
			}
			$_locrs[] = $_locr;
		}
		$returnValue['LocationRequirements'] = $_locrs;

		return $returnValue;
	}

	/**
	 * I assume all quote per location query will return the same columns with aggregated values.
	 *
	 * @var string
	 */
	private static $locationQuoteSQLTemplate = <<<QUOTEBYLOCID_SQL
SELECT qspl.id AS "LocationQuoteId", qp.network AS "ProviderName",
	SUM(CASE WHEN qss.amount_type = 'MRC' THEN qss.amount + qss.markup + qss.discount ELSE 0 END) as "TotalMRC",
	SUM(CASE WHEN qss.amount_type = 'NRC' THEN qss.amount + qss.markup + qss.discount ELSE 0 END) as "TotalNRC",
	SUM(CASE WHEN qss.amount_type = 'MRC' AND LOWER(qss.subclass) = 'local access' THEN qss.amount + qss.markup + qss.discount ELSE 0 END) as "LoopMRC",
	SUM(CASE WHEN qss.amount_type = 'NRC' AND LOWER(qss.subclass) = 'local access' THEN qss.amount + qss.markup + qss.discount ELSE 0 END) as "LoopNRC",
	SUM(CASE WHEN qss.amount_type = 'MRC' AND LOWER(qss.subclass) = 'port' THEN qss.amount + qss.markup + qss.discount ELSE 0 END) as "PortMRC",
	SUM(CASE WHEN qss.amount_type = 'NRC' AND LOWER(qss.subclass) = 'port' THEN qss.amount + qss.markup + qss.discount ELSE 0 END) as "PortNRC",
	SUM(CASE WHEN qss.amount_type = 'MRC' AND LOWER(qss.subclass) = 'router' THEN qss.amount + qss.markup + qss.discount ELSE 0 END) as "RouterMRC",
	SUM(CASE WHEN qss.amount_type = 'NRC' AND LOWER(qss.subclass) = 'router' THEN qss.amount + qss.markup + qss.discount ELSE 0 END) as "RouterNRC",
	SUM(CASE WHEN qss.amount_type = 'MRC' AND LOWER(qss.subclass) = 'qos' THEN qss.amount + qss.markup + qss.discount ELSE 0 END) as "QoSMRC",
	SUM(CASE WHEN qss.amount_type = 'NRC' AND LOWER(qss.subclass) = 'qos' THEN qss.amount + qss.markup + qss.discount ELSE 0 END) as "QoSNRC",
	SUM(CASE WHEN qss.amount_type = 'MRC' AND LOWER(qss.subclass) = 'service' THEN qss.amount + qss.markup + qss.discount ELSE 0 END) as "ServiceMRC",
	SUM(CASE WHEN qss.amount_type = 'NRC' AND LOWER(qss.subclass) = 'service' THEN qss.amount + qss.markup + qss.discount ELSE 0 END) as "ServiceNRC"
	FROM quote_sq_per_loc qspl
	INNER JOIN quote_sq qs ON qspl.quote_sq_id = qs.id
	INNER JOIN rfq_idx ri ON qs.rfq_idx_id = ri.id
	INNER JOIN rfq ON ri.rfq_id = rfq.id
	INNER JOIN quote_providers qp ON qspl.provider_id = qp.id
	INNER JOIN quote_sq_skus qss ON qspl.id = qss.quote_sq_per_loc_id
	----LOCATION_QUOTE_SQL_WHERE_CLAUSE----
	GROUP BY qspl.id, qp.network
QUOTEBYLOCID_SQL;
	
	/**
	 * Visibility of Quote object
	 *
	 * @var Array
	 */
	public static $quoteDataTypes = array(
		'id'                => array('alias' => 'ParentQuoteId',    'read' => true, 'write' => false, 'required' => false ),
		'term'              => array('alias' => 'Term',             'read' => true, 'write' => false, 'required' => false ),
		'quote_complete'    => array('alias' => 'QuoteComplete',    'read' => true, 'write' => false, 'required' => false ),
		'last_updated'      => array('alias' => 'LastUpdated',      'read' => true, 'write' => false, 'required' => false ),
	);

	public static $locQuoteDataTypes = array(
		'id'                 => array('alias' => 'LocationQuoteId',       'read' => true, 'write' => false, 'required' => false ),
		'quote_sq_id'        => array('alias' => 'ParentQuoteId',         'read' => true, 'write' => false, 'required' => false ),
// 		'req_sq_idx_id'      => array('alias' => 'LocationRequirementId', 'read' => true, 'write' => false, 'required' => false ),
		'loc_quote_complete' => array('alias' => 'LocationQuoteComplete', 'read' => true, 'write' => false, 'required' => false ),
		'last_updated'       => array('alias' => 'LastUpdated',           'read' => true, 'write' => false, 'required' => false ),
	);

	public static $locQuoteSkuDataTypes = array(
		'sku'           => array('alias' => 'SKU',          'read' => true, 'write' => false, 'required' => false ),
		'name'          => array('alias' => 'Name',         'read' => true, 'write' => false, 'required' => false ),
		'amount_type'   => array('alias' => 'AmountType',   'read' => true, 'write' => false, 'required' => false ),
		'amount'        => array('alias' => 'Amount',       'read' => true, 'write' => false, 'required' => false ),
		'markup'        => array('alias' => 'Markup',       'read' => true, 'write' => false, 'required' => false ),
		'discount'      => array('alias' => 'Discount',     'read' => true, 'write' => false, 'required' => false ),
		'qty'           => array('alias' => 'Quantity',     'read' => true, 'write' => false, 'required' => false ),
		'group'         => array('alias' => 'Group',        'read' => true, 'write' => false, 'required' => false ),
		'subgroup'      => array('alias' => 'SubGroup',     'read' => true, 'write' => false, 'required' => false ),
		'class'         => array('alias' => 'Class',        'read' => true, 'write' => false, 'required' => false ),
		'subclass'      => array('alias' => 'SubClass',     'read' => true, 'write' => false, 'required' => false ),
		'type'          => array('alias' => 'Type',         'read' => true, 'write' => false, 'required' => false ),
	);

	/**
	 * Used in Quote SKU, subset of $locRequirementDataTypes
	 * @var Array
	 */
	public static $locRequirementDataTypesSKU = array(
		'id'            => array('alias' => 'LocationRequirementId',    'read' => true, 'write' => false, 'required' => false ),
		'req_sq_id'     => array('alias' => 'ParentRequirementId',      'read' => true, 'write' => false, 'required' => false ),
		'location_name' => array('alias' => 'LocationName',             'read' => true, 'write' => false, 'required' => false ),
		'npanxx'        => array('alias' => 'NPANXX',                   'read' => true, 'write' => false, 'required' => false ),
		'address'       => array('alias' => 'Address',                  'read' => true, 'write' => false, 'required' => false ),
		'city'          => array('alias' => 'City',                     'read' => true, 'write' => false, 'required' => false ),
		'state'         => array('alias' => 'State',                    'read' => true, 'write' => false, 'required' => false ),
		'zip'           => array('alias' => 'ZIP',                      'read' => true, 'write' => false, 'required' => false ),
	);

	/**
	 *
	 * @var Array
	 */
	public static $locRequirementDataTypes = array(
		'id'            => array('alias' => 'LocationRequirementId',    'read' => true, 'write' => false, 'required' => false ),
		'req_sq_id'     => array('alias' => 'ParentRequirementId',      'read' => true, 'write' => false, 'required' => false ),
		'location_name' => array('alias' => 'LocationName',             'read' => true, 'write' => false, 'required' => false ),
		'npanxx'        => array('alias' => 'NPANXX',                   'read' => true, 'write' => false, 'required' => false ),
		'address'       => array('alias' => 'Address',                  'read' => true, 'write' => false, 'required' => false ),
		'city'          => array('alias' => 'City',                     'read' => true, 'write' => false, 'required' => false ),
		'state'         => array('alias' => 'State',                    'read' => true, 'write' => false, 'required' => false ),
		'zip'           => array('alias' => 'ZIP',                      'read' => true, 'write' => false, 'required' => false ),

		'location_name_2' => array('alias' => 'LocationName2',          'read' => true, 'write' => false, 'required' => false ),
		'npanxx_2'        => array('alias' => 'NPANXX2',                'read' => true, 'write' => false, 'required' => false ),
		'address_2'       => array('alias' => 'Address2',               'read' => true, 'write' => false, 'required' => false ),
		'city_2'          => array('alias' => 'City2',                  'read' => true, 'write' => false, 'required' => false ),
		'state_2'         => array('alias' => 'State2',                 'read' => true, 'write' => false, 'required' => false ),
		'zip_2'           => array('alias' => 'ZIP2',                   'read' => true, 'write' => false, 'required' => false ),
		'service_status'  => array('alias' => 'ServiceStatus',          'read' => true, 'write' => false, 'required' => false ),
	);

	public static $requirementDataTypes = array(
		'id'            => array('alias' => 'ParentRequirementId',      'read' => true, 'write' => false, 'required' => false ),
		'terms'         => array('alias' => 'Terms',                    'read' => true, 'write' => false, 'required' => false ),
		'timeframe_to_purchase' => array('alias' => 'TimeframeToPurchase', 'read' => true, 'write' => false, 'required' => false ),
		'provider_id_1' => array('alias' => 'Provider1',                'read' => true, 'write' => false, 'required' => false ),
		'provider_id_2' => array('alias' => 'Provider2',                'read' => true, 'write' => false, 'required' => false ),
		'provider_id_3' => array('alias' => 'Provider3',                'read' => true, 'write' => false, 'required' => false ),
		'provider_id_4' => array('alias' => 'Provider4',                'read' => true, 'write' => false, 'required' => false ),
	);

}
