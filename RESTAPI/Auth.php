<?php
/**
 * Assume Database class is loaded in global name space. If it's not, it should
 * be 'required' here, /inc/db/pdo.php
 *
 * @author Simon Yang <syang@ssssoft.com>
 *
 */
namespace RESTAPI;

/**
 * Utility function to authenticate API user. We currently implement
 * HTTP Basic Auth for api applications
 *
 * @author Simon Yang
 *
 */
class Auth {

	/**
	 * Authenticate user against api_token table
	 *
	 * @param unknown_type $toekn
	 * @param unknown_type $secret
	 */
	public static function authenticate($token, $secret) {
		$dbh = \Database::get_connection();
		$token_type_filter = '';
		// $token_type_filter = '"token_type"=\'d\' AND ';
		$sql = 'SELECT * FROM "api_tokens" WHERE ' . $token_type_filter .  '"token" = ? AND "secret" = ?';
		$stmt = $dbh->prepare($sql);
		$stmt->bindValue(1, $token, \PDO::PARAM_STR);
		$stmt->bindValue(2, $secret, \PDO::PARAM_STR);
		$stmt->execute();
		$result = $stmt->fetch(\PDO::FETCH_ASSOC);

		return $result;
	}

	/**
	 * It doesn't belong here, I know. Idon't even use it for initially
	 * populate all secret fields on database. I will move it when I find
	 * a suitable home for it.
	 *
	 * @return String, 32 bit hash for secret of api token
	 */
	public static function getTokenSecretHash() {
		return md5(microtime(true).mt_rand(10000,90000));
	}

}
