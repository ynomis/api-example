<?php

/**
 * Assume Database class is loaded in global name space. If it's not, it should
 * be 'required' here, /inc/db/pdo.php
 *
 * @author Simon Yang <syang@ssssoft.com>
 *
 */

namespace RESTAPI;

/**
 * Facade class for order related actions
 *
 */
class OrdersFacade {

	/**
     * Get order by order_id
     *
     * @param int $macompanyid
     * @param int $orderid
     * @return Array, order data in JSON | failure message
     */
    public function getOrder($macompanyid, $orderid) {
    	$sSql = <<<ORDER_SQL
SELECT
	o.id AS order_id, o.*
FROM orders o
WHERE
	o.ma_company_id = ?
	AND o.id = ?
ORDER_SQL;

    	try {
	    	$dbh = \Database::get_connection();
			$stmt = $dbh->prepare($sSql);
			$stmt->bindValue(1, $macompanyid,	\PDO::PARAM_INT);
			$stmt->bindValue(2, $orderid,		\PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		} catch (\Exception $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Oops, database operation is failed. Please try again later.', 503);
		}

		return $this->orderQuery($results, $macompanyid);
    }

    /**
     * Get orders by client company, optional filters on order status, date
     * period on specified date type.
     *
     * @param $macompanyid, the identity of who makes the request
     * @param $customerid, the client company id with orders to be queried
     * @param $request, \Slim\Http\Request, Slim request object
     *
     * @return Array, order data | failure message
     */
    public function getOrdersByClientCompany($macompanyid, $customerid, $request)
    {
    	$where_clause = '';

    	$status = $request->params('status');
    	if (!empty($status)) {
    		if (in_array($status, self::$acceptableOrderStatuses)) {
    			$where_clause .= "AND o.order_status = '{$status}' ";
    		} else {
    			throw new RESTApiException("Status '$status' is not supported", 400);
    		}
    	}

    	$startDate = $request->params('startDate');
    	$endDate   = $request->params('endDate');
    	$dateType  = $request->params('dateType');
    	if (($startDate && $endDate && $dateType) || (!$startDate && !$endDate && !$dateType)) {
    		if ($startDate && $endDate && $dateType) {
    			if (!array_key_exists($dateType, self::$orderDateTypes)) {
    				throw new RESTApiException("dateType '$dateType' is not supported", 400);
    			}

    			$_datetype = self::$orderDateTypes[$dateType];
    			$where_clause .= "AND o.{$_datetype} BETWEEN '$startDate'::DATE AND '$endDate'::DATE ";
    		}
    	} else {
    		throw new RESTApiException("1 or 2 of startDate, endDate, dateType is missing, they should either all present or none at all.", 400);
    	}

		$sSql = <<<ORDER_SQL
SELECT
	o.id AS order_id, o.*, vf.*
FROM orders o
	INNER JOIN client_companies c
		ON o.client_company_id = c.id
	LEFT JOIN ma_system_setup_order_optional_fields vf
		ON o.ma_company_id = vf.ma_company_id AND o.provider_id = vf.provider_id
WHERE
	c.ma_company_id = ?
	AND c.id = ?
	{$where_clause}
ORDER BY
	o.requested_date DESC, o.id DESC;
ORDER_SQL;

    	try {
	    	$dbh = \Database::get_connection();
			$stmt = $dbh->prepare($sSql);
			$stmt->bindValue(1, $macompanyid,	\PDO::PARAM_INT);
			$stmt->bindValue(2, $customerid,	\PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		} catch (\Exception $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Oops, database operation is failed. Please try again later.', 503);
		}

		return $this->orderQuery($results, $macompanyid);
    }

    /**
     * Get orders by agent id, optional filters on order status, date period
     * on specified date type.
     *
     * @param $macompanyid, the identity of who makes the request
     * @param $agentid, the agentid with orders to be queried
     * @param $request, \Slim\Http\Request, Slim request object
     *
     * @return Array, order data | failure message
     */
    public function getOrdersByAgent($macompanyid, $agentid, $request) {
		// error_log(print_r($request->params('status'), true));
		$where_clause = '';

		$status = $request->params('status');
		if (!empty($status)) {
			if (in_array($status, self::$acceptableOrderStatuses)) {
				$where_clause .= "AND o.order_status = '{$status}' ";
			} else {
				throw new RESTApiException("Status '$status' is not supported", 400);
			}
		}

		$startDate = $request->params('startDate');
		$endDate   = $request->params('endDate');
		$dateType  = $request->params('dateType');
		if (($startDate && $endDate && $dateType) || (!$startDate && !$endDate && !$dateType)) {
			if ($startDate && $endDate && $dateType) {
				if (!array_key_exists($dateType, self::$orderDateTypes)) {
					throw new RESTApiException("dateType '$dateType' is not supported", 400);
				}

				$_datetype = self::$orderDateTypes[$dateType];
				$where_clause .= "AND o.{$_datetype} BETWEEN '$startDate'::DATE AND '$endDate'::DATE ";
			}
		} else {
			throw new RESTApiException("1 or 2 of startDate, endDate, dateType is missing, they should either all present or none at all.", 400);
		}

	    try {
	    	$dbh = \Database::get_connection();
			$stmt = $dbh->prepare("SELECT a.agent_company_id, a.promo_code
				FROM agents a INNER JOIN master_agents ma ON a.ma_id = ma.id
				WHERE ma.ma_company_id = ? AND a.id = ?");
			$stmt->bindValue(1, $macompanyid,	\PDO::PARAM_INT);
			$stmt->bindValue(2, $agentid,		\PDO::PARAM_INT);
			$stmt->execute();
			$aAgent = $stmt->fetch(\PDO::FETCH_ASSOC);
		} catch (\Exception $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Oops, database operation is failed. Please try again later.', 503);
		}

		if (!$aAgent) {
			throw new RESTApiException("Our system can not find agent id <strong>{$agentid}</strong> for the company", 400);
		}

		if (substr($aAgent['promo_code'], 4, 6) == "000000")
		{
			// Agent Administrator
			$sqlAgentRule = "AND a.agent_company_id = {$aAgent['agent_company_id']}";
		}
		elseif (substr($aAgent['promo_code'], 6, 4) == "0000")
		{
			// Sales Manager
			$sqlAgentRule = "AND a.promo_code LIKE '" . substr($aAgent['promo_code'], 0, 6) . "%'";
		}
		else
		{
			// Rep
			$sqlAgentRule = "AND a.promo_code = '{$aAgent['promo_code']}'";
		}

		$sSql = <<<ORDER_SQL
SELECT
	o.id AS order_id, o.*, vf.*
FROM orders o
	INNER JOIN client_companies c
		ON o.client_company_id = c.id
	INNER JOIN agents a
		ON a.id = c.agent_id
	LEFT JOIN ma_system_setup_order_optional_fields vf
		ON o.ma_company_id = vf.ma_company_id AND o.provider_id = vf.provider_id
WHERE
	c.ma_company_id = ?
	AND a.inactive = 'f'
	{$sqlAgentRule}
	{$where_clause}
ORDER BY
	o.requested_date DESC, o.id DESC;
ORDER_SQL;

	try {
			$stmt = $dbh->prepare($sSql);
			$stmt->bindValue(1, $macompanyid,	\PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		} catch (\Exception $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Oops, database operation is failed. Please try again later.', 503);
		}

		return $this->orderQuery($results, $macompanyid);
	}

	/**
	 * Given the actual SQL to query `orders` table, and return result set in
	 * designated format.
	 *
	 * @param Array		$results
	 * @param int		$macompanyid
	 * @return Array,	orders
	 */
	protected function orderQuery($results, $macompanyid) {
		if (!$results) { // Empty result or fail
			return array();
		}

		require_once(DOC_ROOT . '/inc/classes/ma_system_setup_order_tracking.php');
		$benchmarkFieldLabels =
			\MaSystemSetupOrderTracking::getMaSystemSetupOrderTrackingArray($macompanyid);

		$returnValue = array();
		foreach ($results as $record) {
			$_temp = array();
			foreach (self::$orderQueryFields as $column_name => $label) {
				switch ($label['type']) {
					case 'regular' :
						$_temp[$label['alias']] = array(
						'Label'		=> $label['alias'],
						'Value'		=> $record[$column_name],
						);
						break;
					case 'benchmark' :
						$_key = 'label_' . $column_name;
						if (array_key_exists($_key, $benchmarkFieldLabels) && !empty($benchmarkFieldLabels[$_key])) {
							$_temp[$label['alias']] = array(
									'Label'		=> $benchmarkFieldLabels[$_key],
									'Value'		=> $record[$column_name],
							);
						} else {
							$_temp[$label['alias']] = array(
									'Label'		=> $label['alias'],
									'Value'		=> $record[$column_name],
							);
						}
						break;
					case 'vendor' :
						$_show = 'show_' . $column_name;
						$_key  = 'label_' . $column_name;

						if ($record[$_show] === 't') {
							$_temp[$label['alias']] = array(
									'Label'		=> $record[$_key],
									'Value'		=> $record[$column_name],
							);
						}
						break;
					case 'detail-sku' :
						$_temp[$label['alias']] = $this->getOrderSKUs($record['order_id']);

						break;
					default:
						$_temp[$label['alias']] = array(
						'Label'		=> $label['alias'],
						'Value'		=> $record[$column_name],
						);
				}
			}

			$returnValue[] = $_temp;
		}

		return $returnValue;
	}

	/**
	 * Get order id list for a ma company by given date range.
	 * If client company id is provided, filter result on that client company.
	 *
	 * @param int $macompanyid
	 * @param \Slim\Http\Request $request
	 * @throws RESTApiException
	 * @return array
	 */
	public function getOrderIdsByDates($macompanyid, $request) {
		$where_clause = '';
	
		$startDate = $request->params('startDate');
		$endDate   = $request->params('endDate');
		$clientCompanyId = $request->params('customerId');
		// $dateType  = $request->params('dateType');
		$dateType  = 'requested_date';

		if ($startDate && $endDate) {
			$where_clause .= "AND o.{$dateType} BETWEEN '$startDate'::DATE AND '$endDate'::DATE ";
		} else {
			throw new RESTApiException("startDate and/or endDate is missing.", 400);
		}

		if ($clientCompanyId) {
			$where_clause .= "AND o.client_company_id = ? ";
		}

		$sSql = <<<ORDER_SQL
SELECT
	o.id AS order_id
FROM orders o
WHERE
	o.ma_company_id = ?
	{$where_clause}
ORDER BY
	o.id DESC;
ORDER_SQL;

		try {
			$dbh = \Database::get_connection();
			$stmt = $dbh->prepare($sSql);
			$stmt->bindValue(1, $macompanyid,	\PDO::PARAM_INT);
			if ($clientCompanyId)
				$stmt->bindValue(2, $clientCompanyId, \PDO::PARAM_INT);
			$stmt->execute();
			return $stmt->fetchAll(\PDO::FETCH_COLUMN, 0);
		} catch (\PDOException $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Internal errors.', 503);
		}

	}
	
	/**
	 * Query database to get order sku detail for an order.
	 *
	 * @param int $order_id
	 * @return Array
	 */
	protected function getOrderSKUs($order_id) {
		$sku_sql = <<<ORDER_SKU_SQL
SELECT os.*, qspl.api_quote_id
	FROM order_skus os
	LEFT JOIN quote_sq_per_loc qspl ON os.quote_sq_per_loc_id = qspl.id
WHERE os.order_id = ? AND os.order_location_id > 0
ORDER BY os.quote_sq_per_loc_id, os.name
ORDER_SKU_SQL;

		try {
	    	$dbh = \Database::get_connection();
			$stmt = $dbh->prepare($sku_sql);
			$stmt->bindValue(1, $order_id,	\PDO::PARAM_INT);
			$stmt->execute();
			$sku_results = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		} catch (\Exception $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Oops, database operation is failed. Please try again later.', 503);
		}

		$return_value = array();

		foreach ($sku_results as $record) {
			$_format_number = in_array($record['amount_type'], array('MRC', 'NRC'));
			$_skutemp = array();
			foreach (self::$skuQueryFields as $column_name => $labels) {
				if ($_format_number && in_array($column_name, array('amount', 'markup', 'discount'))) {
					$_skutemp[$labels['alias']] = number_format($record[$column_name], 2);
				} else {
					$_skutemp[$labels['alias']] = $record[$column_name];
				}
			}
			$return_value[$record['quote_sq_per_loc_id']][] = $_skutemp;
		}

		return $return_value;
	}

	/**
	 * Get order locations
	 *
	 * @param int $macompanyid
	 * @param int $orderid
	 * @throws RESTApiException
	 * @return Array
	 */
	public function getOrderLocations($macompanyid, $orderid) {
		$sql = <<<LocationSQL
SELECT ol.*, qspl.id AS quote_sq_per_loc_id
	FROM order_locations ol
	INNER JOIN orders o ON ol.order_id = o.id
	LEFT JOIN quote_sq_per_loc qspl ON ol.req_sq_idx_id = qspl.req_sq_idx_id
	WHERE qspl.quote_sq_id = o.quote_id AND o.ma_company_id = ? AND o.id = ?
LocationSQL;

		try {
			$dbh = \Database::get_connection();
			$stmt = $dbh->prepare($sql);
			$stmt->bindValue(1, $macompanyid,	\PDO::PARAM_INT);
			$stmt->bindValue(2, $orderid,		\PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll();
		} catch (\Exception $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Oops, database operation is failed. Please try again later.', 503);
		}

		$return_value = array();
		foreach ($results as $location) {
			$_loc = array();
			foreach (self::$orderLocationDataTypes as $fieldname => $attributes) {
				if ($attributes['read']) {
					$_loc[$attributes['alias']] = $location[$fieldname];
				}
			}
			$return_value[] = $_loc;
		}

		return $return_value;
	}

	/**
	 * Get all user uploaded attachments for an order.
	 *
	 * @param int $macompanyid
	 * @param int $orderid
	 * @throws RESTApiException
	 * @return $file, path to the temporary archive file
	 */
	public function getOrderFiles($macompanyid, $orderid) {
		$dbname = \MSS\BaseModel::connection()->dbname;
		$rootUploadFolder = "/usr/local/uploadfiles/{$dbname}/{$macompanyid}/orders";
		
		$cond = array(
			'conditions' => array("ma_company_id = ? AND key_field = 'order_id' AND key_value = ?", $macompanyid, $orderid),
		);
		try {
			$uploaded = \MSS\Order\UploadFiles::all($cond);
		} catch (\Exception $e) {
			error_log($e->getMessage());
			throw new RESTApiException("Failed in database operations.", 503);
		}

		if (empty($uploaded)) {
			throw new RESTApiException("No attachment found for order [{$orderid}].", 503);
		}
			
		$zip = new \ZipArchive();
		$file = '/usr/local/uploadfiles/TEMP/' . uniqid() . '.zip';
		if ($zip->open($file, \ZipArchive::CREATE) === true) {
			foreach ($uploaded as $f) {
				$realname = $f->filename;
				$fext = strtolower(substr(strrchr($realname,"."),1));
				$diskname = $rootUploadFolder ."/" .str_replace(".$fext", "", $realname)  ."#" .$f->id ."." .$fext;
				
				if (file_exists($diskname)) {
					$zip->addFile($diskname, $realname);
	 			} else {
	 				error_log("This could be a problem, I cannot seem to find the file [{$diskname}]");
	 			}
			}
			$zip->close();
		} else {
			error_log("Can not create archive on [{$file}]");
			throw new RESTApiException("Can not create archive.", 503);
		}

		$limit = 100; // 100 MB
		if (filesize($file) > 100 * 1024 * 1024) {
			unlink($file);
			throw new RESTApiException("Archive generation failed, Because the size of attachments exceeds {$limit} MB.", 503);
		}
		
		return $file;
	}

	/**
	 * As per XO requested, this function is to get quote per location with SKUs
	 *
	 * @param int	$macompanyid
	 * @param Array	$request
	 */
	public function getQuotesPerLocationWithSku($macompanyid, $request) {
		$startDate = $request->params('startDate');
		$endDate   = $request->params('endDate');

		if (empty($startDate) || empty($endDate)) {
			throw new RESTApiException('startDate and endDate is required for the query, please try again.');
		}

		$sql = <<<QUOTE_SQL
SELECT qspl.id as location_id, qspl.*, sku.* FROM quote_sq_skus sku
	INNER JOIN quote_sq_per_loc qspl ON sku.quote_sq_per_loc_id = qspl.id
	INNER JOIN quote_sq sq ON qspl.quote_sq_id = sq.id
	INNER JOIN rfq_idx ri ON sq.rfq_idx_id = ri.id
	INNER JOIN rfq r ON r.id = ri.rfq_id
	WHERE r.ma_company_id = ? AND qspl.last_updated BETWEEN ? AND ? ORDER BY qspl.id;
QUOTE_SQL;

		try {
			$dbh = \Database::get_connection();
			$stmt = $dbh->prepare($sql);
			$stmt->bindValue(1, $macompanyid,	\PDO::PARAM_INT);
			$stmt->bindValue(2, $startDate,		\PDO::PARAM_STR);
			$stmt->bindValue(3, $endDate,		\PDO::PARAM_STR);
			$stmt->execute();
			$quotes = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		} catch (\Exception $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Oops, database operation is failed. Please try again later.', 503);
		}

		$returnValue = array();
		foreach ($quotes as $_quote) {
			$key = $_quote['location_id'];
			if (!array_key_exists($key, $returnValue)) {
				$_tmp = array();
				foreach (self::$quotePerLocationSpecs as $fieldname => $label) {
					$_tmp[$label] = $_quote[$fieldname];
				}
				$_tmp['SKUs'] = array();
				$returnValue[$key] = $_tmp;
			}

			$_tmp = array();
			foreach (self::$quoteSkuSpecs as $fieldname => $label) {
				$_tmp[$label] = $_quote[$fieldname];
			}
			$returnValue[$key]['SKUs'][] = $_tmp;
		}

		return array_values($returnValue);
	}

	/**
	 * Get interview questions and answers for a order by order id
	 *
	 * @param int $macompanyid
	 * @param int $order_id
	 */
	public function getOrderInterviewsByOrderId($macompanyid, $order_id) {
		$sSql = <<<INTERVIEW_SQL
SELECT ois.order_doc_id, oiqs.id AS question_id, oiqs.sequence_id, oiqs.label, oias.answer
	FROM ma_system_setup_order_interview_custom_questions oiqs
	INNER JOIN order_docs odcs ON odcs.sq_doc_id = oiqs.sq_doc_id
	INNER JOIN order_interviews ois ON ois.order_doc_id = odcs.id
	INNER JOIN orders ods ON odcs.quote_sq_id = ods.quote_id
	LEFT JOIN order_interview_answers oias ON ois.id = oias.order_interview_id
		AND oiqs.id = oias.custom_question_id
	WHERE oiqs.ma_company_id = ? AND ods.id = ?
	ORDER BY ois.order_doc_id, oiqs.sequence_id
INTERVIEW_SQL;

		try {
			$dbh = \Database::get_connection();
			$stmt = $dbh->prepare($sSql);
			$stmt->bindValue(1, $macompanyid,	\PDO::PARAM_INT);
			$stmt->bindValue(2, $order_id,		\PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		} catch (\Exception $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Oops, database operation is failed. Please try again later.', 503);
		}

		return $this->processOrderInterviewQuery($results);
	}

	/**
	 * Get interview questions and answers for quote by quote_sq_id
	 *
	 * @param int $macompanyid
	 * @param int $quote_id
	 */
	public function getOrderInterviewsByQuoteId($macompanyid, $quote_id) {
		$sSql = <<<INTERVIEW_SQL
SELECT ois.order_doc_id, oiqs.id AS question_id, oiqs.sequence_id, oiqs.label, oias.answer
	FROM ma_system_setup_order_interview_custom_questions oiqs
	INNER JOIN order_docs odcs ON odcs.sq_doc_id = oiqs.sq_doc_id
	INNER JOIN order_interviews ois ON ois.order_doc_id = odcs.id
	LEFT JOIN order_interview_answers oias ON ois.id = oias.order_interview_id
		AND oiqs.id = oias.custom_question_id
	WHERE oiqs.ma_company_id = ? AND odcs.quote_sq_id = ?
	ORDER BY ois.order_doc_id, oiqs.sequence_id
INTERVIEW_SQL;

		try {
			$dbh = \Database::get_connection();
			$stmt = $dbh->prepare($sSql);
			$stmt->bindValue(1, $macompanyid,	\PDO::PARAM_INT);
			$stmt->bindValue(2, $quote_id,		\PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		} catch (\Exception $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Oops, database operation is failed. Please try again later.', 503);
		}

		return $this->processOrderInterviewQuery($results);
	}

	/**
	 * Digest database query data and make up output for API call.
	 *
	 * @param resource $results
	 * @return Array
	 */
	protected function processOrderInterviewQuery($results) {
		if (!$results) { // Empty result or fail
			return array();
		}

		$_temp = array();
		foreach ($results as $record) {
			$_temp[$record['order_doc_id']][$record['question_id']] =
				array('q' => $record['label'], 'a' => $record['answer']);
		}

		foreach ($_temp as $k => $v) {
			$returnValue[] = array($k => $v);
		}

		return $returnValue;
	}

	public function updateOrderStatus($macompanyid, $order_id, $status) {
		if (!array_key_exists($status, self::$validOrderStatusViaApiUpdate))
			throw new RESTApiException('New status code is not recognized.', 400);
		
		try {
			$order = \MSS\Order::find(
				array('conditions' => array('id = ? AND ma_company_id = ?', $order_id, $macompanyid))
			);
		} catch (\PDOException $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Internal error.', 500);
		}
		
		if (!($order instanceof \MSS\Order))
			throw new RESTApiException('Can not find order with id [' . $order_id . ']', 400);
		
		if ($status == $order->order_status)
			throw new RESTApiException("Order status is already set to '$status'", 400);
		
		// Check if this feature is enabled for the company
		$ma_system_setup = \MSS\MACompany\SystemSetup::find($order->ma_company_id);
		if (!$ma_system_setup->update_order_status_via_api)
			throw new RESTApiException('[Order status update via api] feature may not be enabled for your company.', 400);
		
		$order->order_status = $status;
		$order->last_updated = date('Y-m-d');
		$order->updated_by_id = -1; // none exist user, proposed to represent API
		
		list($on, $off) = self::$validOrderStatusViaApiUpdate[$status];
		
		foreach ($on as $field) {
			if (!$order->$field)
				$order->$field = date('Y-m-d');
		}
		
		foreach ($off as $field) {
			if ($order->$field)
				$order->$field = null;
		}
		
		try {
			if (!$order->save())
				throw new RESTApiException(json_encode($order->errors->full_messages()), 400);

			return 'Successfully updated.';
		} catch(\PDOException $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Internal error', 500);
		}

	}

	public function getClientIdByOrder($macompanyid, $order_id) {
		$sql = <<<ORDER_SQL
SELECT rfq.client_id AS "ClientId", o.client_company_id AS "ClientCompanyId"
	FROM rfq
	INNER JOIN rfq_idx ri ON ri.rfq_id = rfq.id
	INNER JOIN quote_sq qs ON qs.rfq_idx_id = ri.id
	INNER JOIN orders o ON o.quote_id = qs.id
	WHERE o.ma_company_id = ? AND o.id = ?
ORDER_SQL;
		
		try {
			$dbh = \Database::get_connection();
			$stmt = $dbh->prepare($sql);
			$stmt->bindValue(1, $macompanyid,	\PDO::PARAM_INT);
			$stmt->bindValue(2, $order_id,		\PDO::PARAM_INT);
			$stmt->execute();
			return $stmt->fetch(\PDO::FETCH_ASSOC);
		} catch (\PDOException $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Oops, database operation is failed. Please try again later.', 503);
		}
	}

	/**
	 * Update order payment status, used by ANPI only.
	 *
	 * @param int		$macompanyid
	 * @param int		$order_id
	 * @param string	$status
	 * @throws RESTApiException
	 * @return string
	 */
	public function updateOrderPaymentStatus($macompanyid, $order_id, $status) {
		if (!in_array($status, self::$validOrderPaymentStatusViaApiUpdate))
			throw new RESTApiException('New status code is not recognized.', 400);

		try {
			$order = \MSS\Order::find($order_id);
		} catch (\PDOException $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Internal error.', 500);
		}
		
		$payment = $order->payment;
		if ($payment instanceof \MSS\Order\Payment) {
			$payment->status = $status;
			$payment->updated_by	= -1;
			$payment->last_updated	= new \ActiveRecord\DateTime();
			if (!$payment->save())
				throw new RESTApiException(json_encode($payment->errors->full_messages()), 400);
		} else {
			throw new RESTApiException('The order does not have credit card payment setup.', 400);
		}

		return array('success' => true);
	}

	/**
	 * Get order payment info by date range, private to ANPI for now.
	 *
	 * @param int $macompanyid
	 * @param \Slim\Http\Request $request
	 * @throws RESTApiException
	 * @return array
	 */
	public function getOrderPaymentsByDates($macompanyid, $request) {
		$where_clause = '';

		$startDate = $request->params('startDate');
		$endDate   = $request->params('endDate');
		// $clientCompanyId = $request->params('customerId');
		// $dateType  = $request->params('dateType');
		$dateType  = 'requested_date';
		$paymentMethod	= $request->params('paymentMethod');
		$paymentStatus	= $request->params('paymentStatus');
		$docusignStatus	= $request->params('docusignStatus');

		$accepted_docusign_status = array(
			'sent',
			'created',
			'voided',
			'deleted',
			'delivered',
			'signed',
			'completed',
			'declined',
			'timedout',
			'template',
			'processing',
		);
		if ($docusignStatus && !in_array($docusignStatus, $accepted_docusign_status)) {
			throw new RESTApiException('DocuSign status code is not supported.', 400);
		}

		if ($startDate && $endDate) {
			$where_clause .= "AND o.{$dateType} BETWEEN '$startDate'::DATE AND '$endDate'::DATE ";
		} else {
			throw new RESTApiException("startDate and/or endDate is missing.", 400);
		}

		if ($clientCompanyId) {
			$where_clause .= "AND o.client_company_id = :customerid ";
		}
		
		if ($paymentMethod) {
			$where_clause .= "AND p.method = :method ";
		}

		if ($paymentStatus) {
			$where_clause .= "AND p.status = :paymentstatus ";
		}
		
		if ($docusignStatus) {
			$where_clause .= "AND oes.status = :docusignstatus ";
		}

		$sSql = <<<ORDER_SQL
SELECT
	o.id AS "orderId", o.requested_date AS "orderDate", p.method AS "paymentMethod", p.status AS "paymentStatus",
	oes.status AS "docusignStatus"
FROM orders o
INNER JOIN order_extension_esign oes ON o.quote_id = oes.quote_sq_id AND oes.status != 'internally deleted'
INNER JOIN payments p ON o.id = p.order_id
WHERE
	o.ma_company_id = :maid
	{$where_clause}
ORDER BY
	o.id DESC;
ORDER_SQL;

		try {
			$dbh = \Database::get_connection();
			$stmt = $dbh->prepare($sSql);
			$stmt->bindValue(':maid', $macompanyid,	\PDO::PARAM_INT);
			if ($clientCompanyId)
				$stmt->bindValue(':customerid', $clientCompanyId, \PDO::PARAM_INT);
			if ($paymentMethod)
				$stmt->bindValue(':method', $paymentMethod, \PDO::PARAM_STR);
			if ($paymentStatus)
				$stmt->bindValue(':paymentstatus', $paymentStatus, \PDO::PARAM_STR);
			if ($docusignStatus)
				$stmt->bindValue(':docusignstatus', $docusignStatus, \PDO::PARAM_STR);
			$stmt->execute();
			return $stmt->fetchAll(\PDO::FETCH_ASSOC);
		} catch (\PDOException $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Internal errors.', 503);
		} catch (\Exception $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Unknown.', 500);
		}

	}

	/**
	 * Quote per location API spec
	 *
	 */
	public static $quotePerLocationSpecs = array(
		'location_id'			=> 'LocationQuoteId',
		'quote_sq_id'			=> 'ParentQuoteId',
		'req_sq_idx_id'			=> 'RequestId',
		'api_quote_id'			=> 'ApiQuoteId',
		'loc_quote_complete'	=> 'QuoteComplete',
		'last_update'			=> 'LastUpdated',
		'comments'				=> 'QuoteComments',
		'show'					=> 'ShownOnProposal',
	);

	/**
	 * Quote SKU API spec
	 *
	 */
	public static $quoteSkuSpecs = array(
		'name'             => 'Name',
		'qty'              => 'Quantity',
		'amount_type'      => 'AmountType',
		'amount'           => 'Amount',
		'markup'           => 'Markup',
		'discount'         => 'Discount',
		'discount_applied' => 'DiscountPercent',
		'term'             => 'Term',
		'class'            => 'Class',
		'subclass'         => 'SubClass',
		'group'            => 'Group',
		'subgroup'         => 'SubGroup',
		'type'             => 'Type',
		'sub_type'         => 'SubType',
	);

	/**
	 * Acceptable order date type on API query
	 *
	 * @var Array
	 */
	public static $orderDateTypes = array(
		'DueDate'					=> 'due_date',
		'InstalledDate'				=> 'installed_and_sent_to_billing_date',
		'OrderRequestedDate'		=> 'requested_date',
		'RequestedDueDate'			=> 'requested_due_date',
		'TestTurnUpDate'			=> 'test_turn_up_date',
		'SubmittedDate'				=> 'submitted_to_provider_date',
		'SentToProvisioningDate'	=> 'sent_to_provisioning_date',
		'DisconnectDate'			=> 'disconnected_date',
		'FOCDate'					=> 'foc_date'
	);

	/**
	 * Acceptable order status on API query filter
	 *
	 * @var Array
	 */
	public static $acceptableOrderStatuses = array(
		'To Be Scheduled'
		, 'Installed'
		, 'Order Cancelled'
		, 'Pending Install'
		, 'Scheduled'
		, 'Returned to Agent'
		, 'Provisioning'
		, 'Submitted'
		, 'Partially Installed'
		, 'Customer Halted'
		, 'Preparing Order'
		, 'Disconnected'
	);

	/**
	 * Order table column name to requested title mapping
	 *
	 * @var Array
	 */
	public static $orderQueryFields = array (
		// order_id has to be specifically alias-ed in SQL,
		'order_id'                             => array('alias' => 'RecordID',               'type' => 'regular')
		, 'rfq_idx_id'                         => array('alias' => 'rfq_idx_id',             'type' => 'regular')
		, 'quote_id'                           => array('alias' => 'QuoteID',                'type' => 'regular')
		, 'client_company_id'                  => array('alias' => 'CompanyID',              'type' => 'regular')
		, 'assignee_id'                        => array('alias' => 'AssigneeID',             'type' => 'regular')
		, 'order_type'                         => array('alias' => 'OrderType',              'type' => 'regular')
		, 'original_order_id'                  => array('alias' => 'ParentOrderID',          'type' => 'regular')
		, 'provider_id'                        => array('alias' => 'VendorID',               'type' => 'regular')
		, 'network'                            => array('alias' => 'VendorName',             'type' => 'regular')
		, 'product_type'                       => array('alias' => 'ProductType',            'type' => 'regular')
		, 'product_name'                       => array('alias' => 'ProductName',            'type' => 'regular')
		, 'btn'                                => array('alias' => 'BTN',                    'type' => 'regular')
		, 'account_number'                     => array('alias' => 'AccountNumber',          'type' => 'regular')
		, 'order_number'                       => array('alias' => 'OrderNumber',            'type' => 'regular')
		, 'order_name'                         => array('alias' => 'OrderedAsName',          'type' => 'regular')
		, 'requested_date'                     => array('alias' => 'RequestedDate',          'type' => 'benchmark')
		, 'requested_due_date'                 => array('alias' => 'RequestedDueDate',       'type' => 'benchmark')
		, 'submitted_to_provider_date'         => array('alias' => 'OrderSubmittedDate',     'type' => 'benchmark')
		, 'sent_to_credit_date'                => array('alias' => 'SentToCreditDate',       'type' => 'benchmark')
		, 'credit_approved_date'               => array('alias' => 'CreditApprovedDate',     'type' => 'benchmark')
		, 'provider_accepted_date'             => array('alias' => 'VendorAcceptedDate',     'type' => 'benchmark')
		, 'site_survey_date'                   => array('alias' => 'SiteSurveyDate',         'type' => 'benchmark')
		, 'sent_to_provisioning_date'          => array('alias' => 'SentToProvisioningDate', 'type' => 'benchmark')
		, 'installed_and_sent_to_billing_date' => array('alias' => 'InstalledDate',          'type' => 'benchmark')
		, 'foc_date'                           => array('alias' => 'FocDate',                'type' => 'benchmark')
		, 'test_turn_up_date'                  => array('alias' => 'TurnUpDate',             'type' => 'benchmark')
		, 'due_date'                           => array('alias' => 'DueDate',                'type' => 'benchmark')
		, 'disconnected_date'                  => array('alias' => 'DiscoDate',              'type' => 'benchmark')
		, 'misc_date_1'                        => array('alias' => 'MiscDate1',              'type' => 'benchmark')
		, 'misc_date_2'                        => array('alias' => 'MiscDate2',              'type' => 'benchmark')
		, 'misc_date_3'                        => array('alias' => 'MiscDate3',              'type' => 'benchmark')
		, 'misc_date_4'                        => array('alias' => 'MiscDate4',              'type' => 'benchmark')
		, 'misc_date_5'                        => array('alias' => 'MiscDate5',              'type' => 'benchmark')
		, 'misc_date_6'                        => array('alias' => 'MiscDate6',              'type' => 'benchmark')
		, 'misc_date_7'                        => array('alias' => 'MiscDate7',              'type' => 'benchmark')
		, 'misc_date_8'                        => array('alias' => 'MiscDate8',              'type' => 'benchmark')
		, 'misc_date_9'                        => array('alias' => 'MiscDate9',              'type' => 'benchmark')
		, 'misc_date_10'                       => array('alias' => 'MiscDate10',             'type' => 'benchmark')
		, 'misc_date_11'                       => array('alias' => 'MiscDate11',             'type' => 'benchmark')
		, 'user_defined_1'                     => array('alias' => 'UserDefined1',           'type' => 'vendor')
		, 'user_defined_2'                     => array('alias' => 'UserDefined2',           'type' => 'vendor')
		, 'user_defined_3'                     => array('alias' => 'UserDefined3',           'type' => 'vendor')
		, 'user_defined_4'                     => array('alias' => 'UserDefined4',           'type' => 'vendor')
		, 'user_defined_5'                     => array('alias' => 'UserDefined5',           'type' => 'vendor')
		, 'user_defined_6'                     => array('alias' => 'UserDefined6',           'type' => 'vendor')
		, 'user_defined_7'                     => array('alias' => 'UserDefined7',           'type' => 'vendor')
		, 'user_defined_8'                     => array('alias' => 'UserDefined8',           'type' => 'vendor')
		, 'user_defined_9'                     => array('alias' => 'UserDefined9',           'type' => 'vendor')
		, 'user_defined_10'                    => array('alias' => 'UserDefined10',          'type' => 'vendor')
		, 'order_status'                       => array('alias' => 'OrderStatus',            'type' => 'regular')
		, 'last_renewed_date'                  => array('alias' => 'LastRenewedDate',        'type' => 'regular')
		, 'term_renewed'                       => array('alias' => 'TermRenewed',            'type' => 'regular')
		, 'last_updated'                       => array('alias' => 'LastUpdatedDate',        'type' => 'regular')
		, 'est_mo_billing'                     => array('alias' => 'EstMonthlyBill',         'type' => 'regular')
		, 'contract_value'                     => array('alias' => 'ContractValue',          'type' => 'regular')
		, 'cost_mrc'                           => array('alias' => 'MrcCost',                'type' => 'regular')
		, 'markup_mrc'                         => array('alias' => 'MrcMarkup',              'type' => 'regular')
		, 'cost_nrc'                           => array('alias' => 'NrcCost',                'type' => 'regular')
		, 'markup_nrc'                         => array('alias' => 'NrcMarkup',              'type' => 'regular')
		, 'nrc_billed'                         => array('alias' => 'NrcBilled',              'type' => 'regular')
		, 'contract_signed'                    => array('alias' => 'ContractSignedDate',     'type' => 'regular')
		, 'term_starts'                        => array('alias' => 'TermStartDate',          'type' => 'regular')
		, 'term_ends'                          => array('alias' => 'TermEndDate',            'type' => 'regular')
		, 'client_type_1'                      => array('alias' => 'ContactType1',           'type' => 'regular')
		, 'firstname_1'                        => array('alias' => 'FirstName1',             'type' => 'regular')
		, 'lastname_1'                         => array('alias' => 'LastName1',              'type' => 'regular')
		, 'title_1'                            => array('alias' => 'Title1',                 'type' => 'regular')
		, 'phone_1'                            => array('alias' => 'Phone1',                 'type' => 'regular')
		, 'extension_1'                        => array('alias' => 'Ext1',                   'type' => 'regular')
		, 'fax_1'                              => array('alias' => 'Fax1',                   'type' => 'regular')
		, 'email_1'                            => array('alias' => 'Email1',                 'type' => 'regular')
		, 'comments_1'                         => array('alias' => 'Comment1',               'type' => 'regular')
		, 'client_type_2'                      => array('alias' => 'ContactType2',           'type' => 'regular')
		, 'firstname_2'                        => array('alias' => 'FirstName2',             'type' => 'regular')
		, 'lastname_2'                         => array('alias' => 'LastName2',              'type' => 'regular')
		, 'title_2'                            => array('alias' => 'Title2',                 'type' => 'regular')
		, 'phone_2'                            => array('alias' => 'Phone2',                 'type' => 'regular')
		, 'extension_2'                        => array('alias' => 'Ext2',                   'type' => 'regular')
		, 'fax_2'                              => array('alias' => 'Fax2',                   'type' => 'regular')
		, 'email_2'                            => array('alias' => 'Email2',                 'type' => 'regular')
		, 'comments_2'                         => array('alias' => 'Comment2',               'type' => 'regular')
		, 'client_type_3'                      => array('alias' => 'ContactType3',           'type' => 'regular')
		, 'firstname_3'                        => array('alias' => 'FirstName3',             'type' => 'regular')
		, 'lastname_3'                         => array('alias' => 'LastName3',              'type' => 'regular')
		, 'title_3'                            => array('alias' => 'Title3',                 'type' => 'regular')
		, 'phone_3'                            => array('alias' => 'Phone3',                 'type' => 'regular')
		, 'extension_3'                        => array('alias' => 'Ext3',                   'type' => 'regular')
		, 'fax_3'                              => array('alias' => 'Fax3',                   'type' => 'regular')
		, 'email_3'                            => array('alias' => 'Email3',                 'type' => 'regular')
		, 'comments_3'                         => array('alias' => 'Comment3',               'type' => 'regular')
		, 'sku'                                => array('alias' => 'SKUs',                   'type' => 'detail-sku')
	);

	/**
	 * Order sku info [db column name] => [Label], for API use. Should there be
	 * more type of 'query' in above order field list, please follow the
	 * convention to put its key the same as prefix of static array. E.g.
	 * sku maps to $skuQueryFields, otherdetail maps to otherdetailQueryFields
	 *
	 * @var Array
	 */
	public static $skuQueryFields = array (
		'class'              => array('alias' => 'Class'         , 'type' => 'regular'),
		'subclass'           => array('alias' => 'SubClass'      , 'type' => 'regular'),
		'name'               => array('alias' => 'ProductName'   , 'type' => 'regular'),
		'display_group_name' => array('alias' => 'DisplayedName' , 'type' => 'regular'),
		'amount_type'        => array('alias' => 'AmountType'    , 'type' => 'regular'),
		'amount'             => array('alias' => 'Amount'        , 'type' => 'regular'),
		'markup'             => array('alias' => 'Markup'        , 'type' => 'regular'),
		'discount'           => array('alias' => 'Discount'      , 'type' => 'regular'),
		'qty'                => array('alias' => 'Qty'           , 'type' => 'regular'),
		'vendor_id'          => array('alias' => 'VendorID'      , 'type' => 'regular'),
		'vendor_name'        => array('alias' => 'VendorName'    , 'type' => 'regular'),
		'sku'                => array('alias' => 'SKU'           , 'type' => 'regular'),
		'ext_sys_id'         => array('alias' => 'ExternalID'    , 'type' => 'regular'),
		'manually_added'     => array('alias' => 'ManuallyAdded' , 'type' => 'regular'),
		'api_quote_id'       => array('alias' => 'CarrierQuoteId' , 'type' => 'regular'),
	);

	/**
	 * Visibility of order location object
	 *
	 * @var Array
	 */
	public static $orderLocationDataTypes = array(
		'id'                    => array ('alias' => 'OrderLocationId',   'read' => true, 'write' => false, 'required' => false),
		'order_id'              => array ('alias' => 'OrderId',           'read' => true, 'write' => false, 'required' => false),
		'req_sq_idx_id'         => array ('alias' => 'LocationRequestId', 'read' => true, 'write' => false, 'required' => false),
		'quote_sq_per_loc_id'   => array ('alias' => 'LocationQuoteId',   'read' => true, 'write' => false, 'required' => false),
		'npanxx'                => array ('alias' => 'NpaNxx',            'read' => true, 'write' => false, 'required' => false),
		'address'               => array ('alias' => 'Address',           'read' => true, 'write' => false, 'required' => false),
		'building'              => array ('alias' => 'Building',          'read' => true, 'write' => false, 'required' => false),
		'suite'                 => array ('alias' => 'Suite',             'read' => true, 'write' => false, 'required' => false),
		'city'                  => array ('alias' => 'City',              'read' => true, 'write' => false, 'required' => false),
		'state'                 => array ('alias' => 'State',             'read' => true, 'write' => false, 'required' => false),
		'zip'                   => array ('alias' => 'Zip',               'read' => true, 'write' => false, 'required' => false),
		'location_name'         => array ('alias' => 'LocationName',      'read' => true, 'write' => false, 'required' => false),
		'last_four_digits'      => array ('alias' => 'PhoneLastFour',     'read' => true, 'write' => false, 'required' => false),
		'country'               => array ('alias' => 'Country',           'read' => true, 'write' => false, 'required' => false),
		'international'         => array ('alias' => 'International',     'read' => true, 'write' => false, 'required' => false),
	);

	/**
	 * Valid order status via API update.
	 * <status value> => array(<array of fields must have valid value>, <array of must not have values>)
	 *
	 * @var Array
	 */
	public static $validOrderStatusViaApiUpdate = array(
		'Preparing Order'    	=> array(array('requested_date')                                                                                                  , array('submitted_to_provider_date', 'sent_to_provisioning_date', 'installed_and_sent_to_billing_date', 'disconnected_date')),
		'Submitted'          	=> array(array('requested_date', 'submitted_to_provider_date')                                                                    , array('installed_and_sent_to_billing_date', 'disconnected_date')),
		'Pending FOC Date'   	=> array(array('requested_date', 'submitted_to_provider_date')                                                                    , array('installed_and_sent_to_billing_date', 'disconnected_date')),
		'ASR Submitted'      	=> array(array('requested_date', 'submitted_to_provider_date')                                                                    , array('installed_and_sent_to_billing_date', 'disconnected_date')),
		'Pending Install'    	=> array(array('requested_date', 'submitted_to_provider_date')                                                                    , array('installed_and_sent_to_billing_date', 'disconnected_date')),
		'To Be Scheduled'    	=> array(array('requested_date', 'submitted_to_provider_date')                                                                    , array('installed_and_sent_to_billing_date', 'disconnected_date')),
		'Scheduled'          	=> array(array('requested_date', 'submitted_to_provider_date')                                                                    , array('installed_and_sent_to_billing_date', 'disconnected_date')),
		'Provisioning'       	=> array(array('requested_date', 'submitted_to_provider_date', 'sent_to_provisioning_date')                                       , array('installed_and_sent_to_billing_date', 'disconnected_date')),
		'Partially Installed'	=> array(array('requested_date', 'submitted_to_provider_date', 'sent_to_provisioning_date')                                       , array('installed_and_sent_to_billing_date', 'disconnected_date')),
		'Installed'          	=> array(array('requested_date', 'submitted_to_provider_date', 'sent_to_provisioning_date', 'installed_and_sent_to_billing_date') , array('disconnected_date')),
		'Customer Halted'    	=> array(array('requested_date')                                                                                                  , array('installed_and_sent_to_billing_date', 'disconnected_date')),
		'Returned to Agent'  	=> array(array('requested_date')                                                                                                  , array('sent_to_provisioning_date', 'installed_and_sent_to_billing_date', 'disconnected_date')),
		'Order Cancelled'    	=> array(array('requested_date')                                                                                                  , array('submitted_to_provider_date', 'sent_to_provisioning_date', 'installed_and_sent_to_billing_date', 'disconnected_date')),
		'Disconnected'       	=> array(array('requested_date', 'disconnected_date')                                                                             , array('')),
		'Disco Pending'      	=> array(array('requested_date', 'submitted_to_provider_date')                                                                    , array('disconnected_date')),
	);

	/**
	 * Valid order payment status via API update. ANPI only
	 * <status value> => array(<array of fields must have valid value>, <array of must not have values>)
	 *
	 * @var Array
	 */
	public static $validOrderPaymentStatusViaApiUpdate = array(
		'Complete',
	);

}