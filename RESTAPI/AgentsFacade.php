<?php

/**
 * Assume model classes in MSS namespace are loaded, otherwise:
 * require_once(DOC_ROOT . '/models/autoload.php');
 *
 * @author Simon Yang <syang@ssssoft.com>
 *
 */

namespace RESTAPI;

/**
 * Facade class for Agent related actions
 *
 */
class AgentsFacade {

	public function getAgent($macompanyid, $agentId) {
		$join = "INNER JOIN master_agents ma ON  (agents.ma_id = ma.id)";
		$cond = array(	'joins'			=> $join,
						'conditions'	=> array('agents.id = ? AND ma.ma_company_id = ?', $agentId, $macompanyid));

		try {
			$agent = \MSS\Agent::find('first', $cond);
		} catch (\Exception $e) {
			error_log($e->getMessage());
			throw new RESTApiException("Failed in database operation.", 400);
		}

		if (is_null($agent)) {
			throw new RESTApiException("No record found for agent [{$agentId}].", 400);
		}

		$_output = array();
		$_flattened = $agent->to_array();
		foreach (static::$agentDataTypes as $fieldname => $attributes) {
			if ($attributes['read']) {
				$_output[$attributes['alias']] = $_flattened[$fieldname];
			}
		}

		return $_output;
	}

	/**
	 * Visibility of agent object
	 *
	 * @var Array
	 */
	public static $agentDataTypes = array(
		'id'                            => array('alias' => 'AgentId',                'read' => true, 'write' => false, 'required' => false),
		'ma_id'                         => array('alias' => 'AssociateId',            'read' => true, 'write' => false, 'required' => false),
		'agent_company_id'              => array('alias' => 'AgentCompanyId',         'read' => true, 'write' => false, 'required' => false),
		'promo_code'                    => array('alias' => 'PromoCode',              'read' => true, 'write' => false, 'required' => false),
		'reports_to'                    => array('alias' => 'ReportsTo',              'read' => true, 'write' => false, 'required' => false),
		'privileges'                    => array('alias' => 'Privileges',             'read' => false,'write' => false, 'required' => false),
		'type'                          => array('alias' => 'AgentType',              'read' => true, 'write' => false, 'required' => false),
		'firstname'                     => array('alias' => 'FirstName',              'read' => true, 'write' => false, 'required' => false),
		'lastname'                      => array('alias' => 'LastName',               'read' => true, 'write' => false, 'required' => false),
		'title'                         => array('alias' => 'Title',                  'read' => true, 'write' => false, 'required' => false),
		'address'                       => array('alias' => 'Address',                'read' => true, 'write' => false, 'required' => false),
		'city'                          => array('alias' => 'City',                   'read' => true, 'write' => false, 'required' => false),
		'state'                         => array('alias' => 'State',                  'read' => true, 'write' => false, 'required' => false),
		'zip'                           => array('alias' => 'Zip',                    'read' => true, 'write' => false, 'required' => false),
		'phone'                         => array('alias' => 'Phone',                  'read' => true, 'write' => false, 'required' => false),
		'fax'                           => array('alias' => 'Fax',                    'read' => true, 'write' => false, 'required' => false),
		'extension'                     => array('alias' => 'Extension',              'read' => true, 'write' => false, 'required' => false),
		'mobile'                        => array('alias' => 'Mobile',                 'read' => true, 'write' => false, 'required' => false),
		'email'                         => array('alias' => 'Email',                  'read' => true, 'write' => false, 'required' => false),
		'setup_date'                    => array('alias' => 'SetupDate',              'read' => true, 'write' => false, 'required' => false),
		'last_updated'                  => array('alias' => 'LastUpdated',            'read' => true, 'write' => false, 'required' => false),
		'inactive'                      => array('alias' => 'Inactive',               'read' => true, 'write' => false, 'required' => false),
		'inactive_date'                 => array('alias' => 'InactiveDate',           'read' => true, 'write' => false, 'required' => false),
		'comments'                      => array('alias' => 'Comments',               'read' => true, 'write' => false, 'required' => false),
		'quote_ready_email_1st_message' => array('alias' => 'QuoteReadyMsg1',         'read' => false,'write' => false, 'required' => false),
		'quote_ready_email_2nd_message' => array('alias' => 'QuoteReadyMsg2',         'read' => false,'write' => false, 'required' => false),
		'quote_ready_email_salutation'  => array('alias' => 'QuoteReadySalutation',   'read' => false,'write' => false, 'required' => false),
		'quote_ready_email_signoff_as'  => array('alias' => 'QuoteReadySigBlock',     'read' => false,'write' => false, 'required' => false),
		'custom_sidebar_links'          => array('alias' => 'ShowCustomSidebarLinks', 'read' => false,'write' => false, 'required' => false),
		'custom_agent_field_value'      => array('alias' => 'CustomFieldValue',       'read' => false,'write' => false, 'required' => false),
		'comm_view'                     => array('alias' => 'CommViewEnabled',        'read' => false,'write' => false, 'required' => false),
		'show_bulletin_board'           => array('alias' => 'ShowBulletinBoard',      'read' => false,'write' => false, 'required' => false),
		'show_document_library'         => array('alias' => 'ShowDocLibrary',         'read' => false,'write' => false, 'required' => false),
		'country'                       => array('alias' => 'Country',                'read' => true, 'write' => false, 'required' => false),
		'acceptance_terms_of_use'       => array('alias' => 'AcceptedTou',            'read' => true, 'write' => false, 'required' => false),
		'acceptance'                    => array('alias' => 'Acceptance',             'read' => true, 'write' => false, 'required' => false),
		'landing_page'                  => array('alias' => 'LandingPage',            'read' => false,'write' => false, 'required' => false),
		'channel_manager_id'            => array('alias' => 'ChannelManagerId',       'read' => false,'write' => false, 'required' => false),
		'edit_channel_manager'          => array('alias' => 'CanEditChannelManager',  'read' => false,'write' => false, 'required' => false),
		'attribute_1'                   => array('alias' => 'Attribute1',             'read' => true, 'write' => false, 'required' => false),
		'attribute_2'                   => array('alias' => 'Attribute2',             'read' => true, 'write' => false, 'required' => false),
		'attribute_3'                   => array('alias' => 'Attribute3',             'read' => true, 'write' => false, 'required' => false),
	);

}