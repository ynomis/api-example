<?php

/**
 * Assume model classes in MSS namespace are loaded, otherwise:
 * require_once(DOC_ROOT . '/models/autoload.php');
 *
 * @author Simon Yang <syang@ssssoft.com>
 *
 */

namespace RESTAPI;

/**
 * Facade class for client related actions
 *
 */
class ClientsFacade {

	/**
	 * @param int $macompanyid
	 * @param int $clientid
	 * @return string, JSON representation of client object
	 *
	 */
	public function getClient($macompanyid, $clientid) {
		try {
			$client = \MSS\Client::find($clientid);
		} catch (\ActiveRecord\RecordNotFound $e) {
			throw new RESTApiException("No record found for client [{$clientid}] in your company.", 400);
		} catch (\Exception $e) {
			error_log($e->getMessage());
			throw new RESTApiException('Failed in database operation.', 400);
		}

		$clco = $client->client_company;

		if ($clco->ma_company_id != $macompanyid) {
			throw new RESTApiException("No record found for client [{$clientid}] in your company.", 400);
		}

		$_output = array();
		$_flattened = $client->to_array();
		foreach (static::$clientDataTypes as $fieldname => $attributes) {
			if ($attributes['read']) {
				$_output[$attributes['alias']] = $_flattened[$fieldname];
			}
		}

		return json_encode($_output);
	}

	/**
	 * To look up a client in a client company.
	 * In case more db fields are invloved, add them into $valid_query_params
	 *
	 * @param int $macompanyid
	 * @param int $clientcompanyid
	 * @param \Slim\Http\Request $request
	 * @throws RESTApiException
	 * @return string
	 */
	public function findClient($macompanyid, $clientcompanyid, $request) {
		//�maps from query name to db name, array([URL_query name] => [DB field name])
		$valid_query_params = array('email' => 'LOWER(email)');
		$params = $request->get();
		$intersect = array_intersect_key($params, $valid_query_params);
		
		if (empty($intersect))
			throw new RESTApiException('No acceptable query parameters received.', 400);

		$query_where	= 'client_company_id = ? AND client_companies.ma_company_id = ?';
		$query_values	= array($clientcompanyid, $macompanyid);
		foreach ($intersect as $key => $value) {
			$query_where .= " AND {$valid_query_params[$key]} = ?";
			if (strpos($valid_query_params[$key], 'LOWER(') !== false)
				$value = strtolower($value);
			array_push($query_values, $value);
		}
		array_unshift($query_values, $query_where);

		try {
			$client = \MSS\Client::first(array(
				'conditions'	=> $query_values,
				'joins'			=> array('client_company'),
			));
		} catch (\PDOException $e) {
			throw new RESTApiException('Internal Error.', 500);
		}
		
		if (!($client instanceof \MSS\Client))
			throw new RESTApiException('No client found by given parameters.');

		$_output = array();
		$_flattened = $client->to_array();
		foreach (static::$clientDataTypes as $fieldname => $attributes) {
			if ($attributes['read']) {
				$_output[$attributes['alias']] = $_flattened[$fieldname];
			}
		}

		return json_encode($_output);

	}
	
	public function updateClient($macompanyid, $clientid, $request) {
		$cond = array(
			'conditions'	=> array('clients.id = ? AND client_companies.ma_company_id = ?', $clientid, $macompanyid),
            'joins'			=> array('client_company'),
		);
		$client = \MSS\Client::first($cond);

		if ($client instanceof \MSS\Client) {
			$params = $request->put();

			foreach (static::$clientDataTypes as $fieldname => $attributes) {
				$value = trim($params[$attributes['alias']]);
				if ($attributes['write'] && strlen($value) > 0) {
					$client->{$fieldname} = $value;
				}
			}

			if (!$client->save()) {
				return array('errors' => $client->errors->full_messages());
		    } else {
		    	return array('success' => true);
		    }
		} else {
			return array('errors' => array("Can not find client [{$clientid}] within your company."));
		}
	}

	/**
	 * Add a new client to given client company
	 *
	 * @param int $macompanyid
	 * @param mixed $clientcompany, it now expects ClientCompany instance, or client company id in integer.
	 * @param object $request
	 * @param string $prefix
	 * @return multitype:multitype:string  |multitype:Ambigous <multitype:, multitype:unknown > |multitype:boolean NULL
	 */
	public function addClient($macompanyid, $clientcompany, $request, $prefix = '') {
		if ($clientcompany instanceof \MSS\ClientCompany) {
			if (empty($clientcompany->id) || $clientcompany->ma_company_id != $macompanyid) {
				return array('errors' => array("Can not find the customer in your company."));
			}

			$clientcompany_id = $clientcompany->id;
		
		} else {
			if (!\MSS\ClientCompany::exists(
				array('conditions' => array('id = ? AND ma_company_id = ?', $clientcompany, $macompanyid)))) {
				return array('errors' => array("Can not find customer [{$clientcompany}] in your company."));
			}

			$clientcompany_id = $clientcompany;
		}

		$client = new \MSS\Client;

		$client->client_company_id	= $clientcompany_id;
		$client->inactive			= false;

		$params = $request->post();

		foreach (static::$clientDataTypes as $fieldname => $attributes) {
			$value = trim($params[$prefix . $attributes['alias']]);
			if ($attributes['write'] && strlen($value) > 0) {
				$client->{$fieldname} = $value;
			}
		}

		if (!$client->save()) {
			return array('errors' => $client->errors->full_messages());
	    } else {
	    	return array('success' => true, 'clientId' => $client->id);
	    }
	}

	/**
	 * Visibility of client object
	 *
	 * @var Array
	 */
	public static $clientDataTypes = array(
		'id'                       => array('alias' => 'ID',                    'read' => true, 'write' => false, 'required' => false ),
		'client_company_id'        => array('alias' => 'CompanyID',             'read' => true, 'write' => false, 'required' => true ),
		'surname_title'            => array('alias' => 'Surname',               'read' => true, 'write' => true,  'required' => false ),
		'firstname'                => array('alias' => 'FirstName',             'read' => true, 'write' => true,  'required' => true ),
		'lastname'                 => array('alias' => 'LastName',              'read' => true, 'write' => true,  'required' => true ),
		'title'                    => array('alias' => 'Title',                 'read' => true, 'write' => true,  'required' => false ),
		'phone'                    => array('alias' => 'Phone',                 'read' => true, 'write' => true,  'required' => true ),
		'extension'                => array('alias' => 'PhoneExt',              'read' => true, 'write' => true,  'required' => false ),
		'alt_phone'                => array('alias' => 'AltPhone',              'read' => true, 'write' => true,  'required' => false ),
		'mobile'                   => array('alias' => 'MobilePhone',           'read' => true, 'write' => true,  'required' => false ),
		'fax'                      => array('alias' => 'Fax',                   'read' => true, 'write' => true,  'required' => false ),
		'email'                    => array('alias' => 'Email',                 'read' => true, 'write' => true,  'required' => true ),
		'location_id'              => array('alias' => 'LocID',                 'read' => true, 'write' => false, 'required' => false ),
		'inactive'                 => array('alias' => 'Inactive',              'read' => true, 'write' => true,  'required' => false ),
		'inactive_date'            => array('alias' => 'InactiveDate',          'read' => true, 'write' => false, 'required' => false ),
		'comments'                 => array('alias' => 'Comment',               'read' => true, 'write' => true,  'required' => false ),
		'preferred_contact_method' => array('alias' => 'PreferredContactMethod','read' => true, 'write' => true,  'required' => false ),
	);

}