<?php

/**
 * Assume model classes in MSS namespace are loaded, otherwise:
 * require_once(DOC_ROOT . '/models/autoload.php');
 *
 * @author Simon Yang <syang@ssssoft.com>
 *
 */

namespace RESTAPI;

/**
 * Facade class for client company related actions
 *
 */
class ClientCompaniesFacade {

	const PREFIX_NEW_CLIENT_IN_COMPANY = 'clt_';

	public function getClientCompany($macompanyid, $customerid) {
		$cond = array('conditions' => array('id = ? AND ma_company_id = ?', $customerid, $macompanyid));
		try {
			$clientCompany = \MSS\ClientCompany::find('first', $cond);
		} catch (\Exception $e) {
			error_log($e->getMessage());
			throw new RESTApiException("Can not find customer [{$customerid}] in your company.", 400);
		}

		if (is_null($clientCompany)) {
			throw new RESTApiException("No record found for client company [{$customerid}].", 400);
		}

		$_output = array();
		$_flattened = $clientCompany->to_array();
		foreach (static::$clientCompanyDataTypes as $fieldname => $attributes) {
			if ($attributes['read']) {
				$_output[$attributes['alias']] = $_flattened[$fieldname];
			}
		}

		return json_encode($_output);
	}

	/**
	 * Look up a client company, by company name. BUt it could be extended to other field
	 * criteria. In such case, add more field definitions into $valid_query_params
	 *
	 * @param int $macompanyid
	 * @param \Slim\Http\Request $request
	 * @throws RESTApiException
	 * @return string
	 */
	public function findClientCompany($macompanyid, $request) {
		$pattern = '[^0-9a-zA-Z& ]';
		//�maps from query name to db name, array([URL_query name] => [DB field name])
		$valid_query_params = array('companyName' => "REGEXP_REPLACE(LOWER(company_name), '{$pattern}', '')");
		$params = $request->get();
		$intersect = array_intersect_key($params, $valid_query_params);
		
		if (empty($intersect))
			throw new RESTApiException('No acceptable query parameters received.', 400);
		
		$query_where	= 'ma_company_id = ?';
		$query_values	= array($macompanyid);
		foreach ($intersect as $key => $value) {
			$query_where .= " AND {$valid_query_params[$key]} = ?";
			if (strpos($valid_query_params[$key], 'LOWER(') !== false)
				$value = strtolower($value);
			if (strpos($valid_query_params[$key], 'REGEXP_REPLACE(') !== false)
				$value = preg_replace("/{$pattern}/", '', $value);
			array_push($query_values, $value);
		}
		array_unshift($query_values, $query_where);
		
		try {
			$clientCompany = \MSS\ClientCompany::first(array(
				'conditions'	=> $query_values,
			));
		} catch (\PDOException $e) {
			throw new RESTApiException('Internal Error.', 500);
		}
		
		if (!($clientCompany instanceof \MSS\ClientCompany))
			throw new RESTApiException('No client company found by given parameters.');

		$_output = array();
		$_flattened = $clientCompany->to_array();
		foreach (static::$clientCompanyDataTypes as $fieldname => $attributes) {
			if ($attributes['read']) {
				$_output[$attributes['alias']] = $_flattened[$fieldname];
			}
		}

		return json_encode($_output);
	}
	
	public function updateClientCompany($macompanyid, $customerid, $request) {
		$cond = array('conditions' => array('id = ? AND ma_company_id = ?', $customerid, $macompanyid));
		$clientCompany = \MSS\ClientCompany::first($cond);

		if ($clientCompany instanceof \MSS\ClientCompany) {
			$params = $request->put();

			foreach (static::$clientCompanyDataTypes as $fieldname => $attributes) {
				$value = trim($params[$attributes['alias']]);
				if ($attributes['write'] && strlen($value) > 0) {
					$clientCompany->{$fieldname} = $value;
				}
			}

			$clientCompany->api_updated	= date('Y-m-d');

			if (!$clientCompany->save()) {
				return array('errors' => $clientCompany->errors->full_messages());
		    } else {
		    	return array('success' => true);
		    }
		} else {
			return array('errors' => array("Can not find client company [{$customerid}] within your company."));
		}
	}

	public function addClientCompany($macompanyid, $agentid, $request) {
		$cond = array(
			'conditions'	=> array('agents.id = ? AND master_agents.ma_company_id = ?', $agentid, $macompanyid),
            'joins'			=> array('master_agent'),
		);

		if (!\MSS\Agent::exists($cond)) {
			return array('errors' => array("Can not find agent [{$agentid}] in your company."));
		}

		$clientCompany = new \MSS\ClientCompany;
		$clientCompany->ma_company_id	= $macompanyid;
		$clientCompany->agent_id		= $agentid;
		$clientCompany->inactive		= false;
		$clientCompany->company_type	= 'Suspect';
		$clientCompany->api_updated		= date('Y-m-d');

		$params = $request->post();

		foreach (static::$clientCompanyDataTypes as $fieldname => $attributes) {
			$value = trim($params[$attributes['alias']]);
			if ($attributes['write'] && strlen($value) > 0) {
				$clientCompany->{$fieldname} = $value;
			}
		}

    	try {
			if(!$clientCompany->save()) {
				return array('errors' => $clientCompany->errors->full_messages());
			}

    		// Now try to create a new client
	    	$clientFacade = new ClientsFacade();
	    	$result = $clientFacade->addClient(
	    		$macompanyid, $clientCompany, $request, self::PREFIX_NEW_CLIENT_IN_COMPANY);

	    	if ($result['success']) {
	    		// successfully created a master user of the new client company
		    	return array( 'success'		=> true,
		    				  'customerId'	=> $clientCompany->id,
		    				  'clientId'	=> $result['clientId'] );
	    	} else {
	    		throw new \Exception('Failed to create a new client');
	    	}
    	} catch (\Exception $e) { // catch whatever error, and remove the temporary client company
    		error_log($e->getMessage());

			// if creating client failed, the newly created client company needs to be removed.
			// Cannot use $clientCompany->delete() here, 'cause it will trigger active client check
			// which does not apply here.
			if ($clientCompany->id) {
				$count = $clientCompany->delete_all(
					array('conditions' => array('id = ?', $clientCompany->id))
				);
				if (!$count) {
					error_log('Why can\'t I delete this client company [' . $clientCompany->id . '] here?');
				}
			}

			return array('errors' => 'Unidentified error');
    	}
	}

	/**
	 * Visibility of client company object
	 *
	 * @var Array
	 */
	public static $clientCompanyDataTypes = array(
		'id'             => array('alias' => 'ID',                  'read' => true, 'write' => false, 'required' => false),
		'agent_id'       => array('alias' => 'AgentID',             'read' => true, 'write' => false, 'required' => true),
		'company_name'   => array('alias' => 'CompanyName',         'read' => true, 'write' => true,  'required' => true),
		'address'        => array('alias' => 'Address',             'read' => true, 'write' => true,  'required' => true),
		'suite'          => array('alias' => 'Address2',            'read' => true, 'write' => true,  'required' => false),
		'city'           => array('alias' => 'City',                'read' => true, 'write' => true,  'required' => true),
		'state'          => array('alias' => 'State',               'read' => true, 'write' => true,  'required' => true),
		'zip'            => array('alias' => 'Zip',                 'read' => true, 'write' => true,  'required' => true),
		'source'         => array('alias' => 'Source',              'read' => true, 'write' => true,  'required' => false),
		'general_phone'  => array('alias' => 'GeneralPhone',        'read' => true, 'write' => true,  'required' => false),
		'general_fax'    => array('alias' => 'GeneralFax',          'read' => true, 'write' => true,  'required' => false),
		'company_url'    => array('alias' => 'Website',             'read' => true, 'write' => true,  'required' => false),
		'est_mo_revenue' => array('alias' => 'EstimatedMonthlyRev', 'read' => true, 'write' => true,  'required' => false),
		'signup_date'    => array('alias' => 'CreatedDate',         'read' => true, 'write' => false, 'required' => false),
		'inactive'       => array('alias' => 'Inactive',            'read' => true, 'write' => false, 'required' => false),
		'inactive_date'  => array('alias' => 'InactiveDate',        'read' => true, 'write' => false, 'required' => false),
		'lead'           => array('alias' => 'IsLead',              'read' => true, 'write' => true,  'required' => true),
		'comment'        => array('alias' => 'Comment',             'read' => true, 'write' => true,  'required' => false),
		'attribute_1'    => array('alias' => 'CustomField1',        'read' => true, 'write' => true,  'required' => false),
		'attribute_2'    => array('alias' => 'CustomField2',        'read' => true, 'write' => true,  'required' => false),
		'attribute_3'    => array('alias' => 'CustomField3',        'read' => true, 'write' => true,  'required' => false),
	);

}