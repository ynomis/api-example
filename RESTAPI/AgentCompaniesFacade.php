<?php

/**
 * Assume model classes in MSS namespace are loaded, otherwise:
 * require_once(DOC_ROOT . '/models/autoload.php');
 *
 * @author Simon Yang <syang@ssssoft.com>
 *
 */

namespace RESTAPI;

/**
 * Facade class for Agent related actions
 *
 */
class AgentCompaniesFacade {

	public function getAgentCompany($macompanyid, $agentCompanyId) {
		$join = "INNER JOIN master_agents ma ON (agent_companies.ma_id = ma.id)";
		$cond = array(	'joins'			=> $join,
						'conditions'	=> array('agent_companies.id = ? AND ma.ma_company_id = ?', $agentCompanyId, $macompanyid));

		try {
			$agentCompany = \MSS\AgentCompany::find('first', $cond);
		} catch (\Exception $e) {
			error_log($e->getMessage());
			throw new RESTApiException("Failed in database operation.", 400);
		}

		if (is_null($agentCompany)) {
			throw new RESTApiException("No record found for agent company [{$agentCompanyId}].", 400);
		}

		$_output = array();
		$_flattened = $agentCompany->to_array();
		foreach (static::$agentCompanyDataTypes as $fieldname => $attributes) {
			if ($attributes['read']) {
				$_output[$attributes['alias']] = $_flattened[$fieldname];
			}
		}

		return $_output;
	}

	/**
	 * Visibility of agent company object
	 *
	 * @var Array
	 */
	public static $agentCompanyDataTypes = array(
		'id'                                        => array( 'alias' => 'AgentCompanyId',                'read' => true, 'write' => false, 'required' => false ),
		'ma_id'                                     => array( 'alias' => 'AssociateID',                   'read' => true, 'write' => false, 'required' => false ),
		'promo_code_4'                              => array( 'alias' => 'PromoCodeRoot',                 'read' => true, 'write' => false, 'required' => false ),
		'legal_company_name'                        => array( 'alias' => 'LegalCompanyName',              'read' => true, 'write' => false, 'required' => false ),
		'company_name'                              => array( 'alias' => 'CompanyName',                   'read' => true, 'write' => false, 'required' => false ),
		'company_url'                               => array( 'alias' => 'CompanyUrl',                    'read' => false,'write' => false, 'required' => false ),
		'tax_id'                                    => array( 'alias' => 'TaxId',                         'read' => false,'write' => false, 'required' => false ),
		'firstname'                                 => array( 'alias' => 'FirstName',                     'read' => true, 'write' => false, 'required' => false ),
		'lastname'                                  => array( 'alias' => 'LastName',                      'read' => true, 'write' => false, 'required' => false ),
		'phone'                                     => array( 'alias' => 'Phone',                         'read' => true, 'write' => false, 'required' => false ),
		'fax'                                       => array( 'alias' => 'Fax',                           'read' => true, 'write' => false, 'required' => false ),
		'address'                                   => array( 'alias' => 'Address',                       'read' => true, 'write' => false, 'required' => false ),
		'suite'                                     => array( 'alias' => 'Suite',                         'read' => true, 'write' => false, 'required' => false ),
		'city'                                      => array( 'alias' => 'City',                          'read' => true, 'write' => false, 'required' => false ),
		'state'                                     => array( 'alias' => 'State',                         'read' => true, 'write' => false, 'required' => false ),
		'zip'                                       => array( 'alias' => 'Zip',                           'read' => true, 'write' => false, 'required' => false ),
		'rfq_bank'                                  => array( 'alias' => 'RfqBank',                       'read' => false,'write' => false, 'required' => false ),
		'update_interval'                           => array( 'alias' => 'UpdateInterval',                'read' => false,'write' => false, 'required' => false ),
		'employees'                                 => array( 'alias' => 'Employees',                     'read' => false,'write' => false, 'required' => false ),
		'comments'                                  => array( 'alias' => 'Comments',                      'read' => true, 'write' => false, 'required' => false ),
		'last_updated'                              => array( 'alias' => 'LastUpdated',                   'read' => true, 'write' => false, 'required' => false ),
		'updated_by_id'                             => array( 'alias' => 'UpdatedById',                   'read' => true, 'write' => false, 'required' => false ),
		'setup_date'                                => array( 'alias' => 'SetupDate',                     'read' => true, 'write' => false, 'required' => false ),
		'inactive'                                  => array( 'alias' => 'Inactive',                      'read' => true, 'write' => false, 'required' => false ),
		'inactive_date'                             => array( 'alias' => 'InactiveDate',                  'read' => true, 'write' => false, 'required' => false ),
		'inactive_reason'                           => array( 'alias' => 'InactiveReason',                'read' => false,'write' => false, 'required' => false ),
		'inactive_notes'                            => array( 'alias' => 'InactiveNotes',                 'read' => false,'write' => false, 'required' => false ),
		'receive_rfq_submission'                    => array( 'alias' => 'ReceiveRfqNotification',        'read' => false,'write' => false, 'required' => false ),
		'agency_type'                               => array( 'alias' => 'AgencyType',                    'read' => true, 'write' => false, 'required' => false ),
		'new_agent_user_notification'               => array( 'alias' => 'NotificationOnNewAgent',        'read' => false,'write' => false, 'required' => false ),
		'agt_comm_view'                             => array( 'alias' => 'CommViewEnabled',               'read' => false,'write' => false, 'required' => false ),
		'show_network_in_agt_service_request_forms' => array( 'alias' => 'ShowVendorsInRfqForm',          'read' => false,'write' => false, 'required' => false ),
		'show_network_in_agt_quote_manager'         => array( 'alias' => 'ShowVendorsInQuoteMgr',         'read' => false,'write' => false, 'required' => false ),
		'allow_manual_order_creation'               => array( 'alias' => 'AllowManualOrderCreation',      'read' => false,'write' => false, 'required' => false ),
		'show_excluded_in_qms'                      => array( 'alias' => 'ShowExcludedVendorsInQuoteMgr', 'read' => false,'write' => false, 'required' => false ),
		'show_document_library'                     => array( 'alias' => 'ShowDocLibrary',                'read' => false,'write' => false, 'required' => false ),
		'hide_proposal_signature'                   => array( 'alias' => 'HideProposalSignature',         'read' => false,'write' => false, 'required' => false ),
		'bizorgid'                                  => array( 'alias' => 'ExternalOrganizationId',        'read' => true, 'write' => false, 'required' => false ),
		'auto_ready'                                => array( 'alias' => 'AutoReadyForQuotes',            'read' => false,'write' => false, 'required' => false ),
		'auto_ready_min_provider_quoted'            => array( 'alias' => 'AutoReadyMinProviderQty',       'read' => false,'write' => false, 'required' => false ),
		'edit_requirements'                         => array( 'alias' => 'EditRequirements',              'read' => false,'write' => false, 'required' => false ),
		'edit_locations'                            => array( 'alias' => 'EditLocations',                 'read' => false,'write' => false, 'required' => false ),
		'resubmit'                                  => array( 'alias' => 'ResubmitEnabled',               'read' => false,'write' => false, 'required' => false ),
		'service_order'                             => array( 'alias' => 'ServiceOrder',                  'read' => false,'write' => false, 'required' => false ),
		'country'                                   => array( 'alias' => 'Country',                       'read' => true, 'write' => false, 'required' => false ),
		'attribute_1'                               => array( 'alias' => 'Attribute1',                    'read' => true, 'write' => false, 'required' => false ),
		'attribute_2'                               => array( 'alias' => 'Attribute2',                    'read' => true, 'write' => false, 'required' => false ),
		'attribute_3'                               => array( 'alias' => 'Attribute3',                    'read' => true, 'write' => false, 'required' => false ),
		'view_files'                                => array( 'alias' => 'ViewFiles',                     'read' => false,'write' => false, 'required' => false ),
		'upload_files'                              => array( 'alias' => 'UploadFiles',                   'read' => false,'write' => false, 'required' => false ),
		'quote_sq_display_basis'                    => array( 'alias' => 'DisplayBasis',                  'read' => false,'write' => false, 'required' => false ),
		'non_catalog_quote_entry'                   => array( 'alias' => 'NonCatalogQuoteEntryEnabled',   'read' => false,'write' => false, 'required' => false ),
		'doc_manager'                               => array( 'alias' => 'DocumentManager',               'read' => false,'write' => false, 'required' => false ),
		'sq_add_edit_quotes'                        => array( 'alias' => 'AddEditQuotesEnabled',          'read' => false,'write' => false, 'required' => false ),
		'sq_delete_quotes'                          => array( 'alias' => 'DeleteQuotesEnabled',           'read' => false,'write' => false, 'required' => false ),
		'sq_set_to_working'                         => array( 'alias' => 'SetToWorkingStatusEnabled',     'read' => false,'write' => false, 'required' => false ),
		'sq_update_show'                            => array( 'alias' => 'UpdateShowEnabled',             'read' => false,'write' => false, 'required' => false ),
	);

}