<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>MSS REST API Documentation | Get PowerQuotes</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/main.css">
	<script src="js/vendor/modernizr-2.6.2.min.js"></script>

	<link rel="stylesheet" href="css/help.css">
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

<!-- Add your site or application content here -->
<h1 class="helpHead1">
	<a name="heading_1_1"><!-- --></a><span class="ph" id="topic-title"><a name="topic-title"><!-- --></a>Get PowerQuotes</span></h1>
<div class="body">
	<p class="p">
		Get all quotes, with their SKUs. <span style="color:#ff0000">Special permission required to use this endpoint.</span></p>
	<dl class="dl">
		<dt class="dt">
			<a name=""><!-- --></a>URI</dt>
		<dd class="dd">
			<samp class="codeph nolang"><samp class="codeph nolang"><samp class="codeph nolang"><samp class="codeph nolang">/powerquotes/</samp>&lt;rfq_number&gt;</samp></samp></samp></dd>
		<dt class="dt">
			<a name=""><!-- --></a>Result Formats</dt>
		<dd class="dd">
			JSON</dd>
		<dt class="dt">
			<a name=""><!-- --></a>HTTP Method</dt>
		<dd class="dd">
			GET</dd>
		<dt class="dt">
			<a name=""><!-- --></a>Authentication</dt>
		<dd class="dd">
			<samp class="codeph nolang">Authorization: Basic Auth token:secret</samp></dd>
		<dt class="dt">
			<a name=""><!-- --></a>URL Query Parameters</dt>
		<dd class="dd">None</dd>
		<dt class="dt">
			<a name="order_result_fields"><!-- --></a>Return Result</dt>
		<dd class="dd">
			The result is represented as a JSON string:

<pre>
{	"rfqStatus":"Completed",
	"quotes":
		[{	"term_months":12,
			"providers":
				[{	"provider_id":1678,
					"provider_name":"Provider name",
					"locations":
						[{	"quote_location_id":415814,
							"location_info":
								{	"id":"14614",
									"req_sq_id":"10157",
									"location_name":"Loc 1",
									"npanxx":"555555",
									"address":"999 BROADWAY",
									"city":"NEW YORK",
									"state":"NY",
									"zip":"10010",
									"input_value_1":"Internet",
									"input_value_3":"10M FastE",
									"input_value_4":"10",
									"input_value_10":"No",
									"read":"f",
									"service_status":"Ready",
									"became_ready_on":"2012-06-22 14:16:20",
									"status_last_updated":"2012-06-22 14:16:20",
									"status_updated_by_id":"0"
								},
							"skus":
								[{	"sku":"DT-LA-DI-10MEOC-12-PROVIDER1678-ALL",
									"name":"DI ETHERNET OVER COPPER LOOP 10",
									"amount":434,
									"amount_type":"MRC",
									"subclass":"Local Access",
									"subgroup":"10M EoC"
								},
								{	"sku":"DT-LA-DI-10MEOC-12-PROVIDER678-ALL",
									"name":"DI ETHERNET OVER COPPER LOOP 10",
									"amount":500,
									"amount_type":"NRC",
									"subclass":"Local Access",
									"subgroup":"10M EoC"
								},
								{	"sku":"DT-PO-DI-10MEOC-12-PROVIDER1678-ALL",
									"name":"DI ETHERNET PORT 10",
									"amount":80,
									"amount_type":"MRC",
									"subclass":"Port",
									"subgroup":"10M EoC"
								}]
						}]
				}]
		}]
}
</pre>
		</dd>
	</dl>
</div>



<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.0.min.js"><\/script>')</script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>

</body>
</html>
