<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>MSS REST API Documentation | Quote By Location Id</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>

        <link rel="stylesheet" href="css/help.css">
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
		<h1 class="helpHead1">
			<a name="heading_1_1"><!-- --></a><span class="ph" id="topic-title"><a name="topic-title"><!-- --></a>Quote By Location Id</span></h1>
		<div class="body">
			<p class="p">
				Get Quote for a specific location by its id.</p>
			<dl class="dl">
				<dt class="dt">
					<a name=""><!-- --></a>URI</dt>
				<dd class="dd">
					<samp class="codeph nolang"><samp class="codeph nolang"><samp class="codeph nolang"><samp class="codeph nolang">/locquote/</samp>&lt;location_quote_id&gt;</samp></samp></samp></dd>
				<dt class="dt">
					<a name=""><!-- --></a>Result Formats</dt>
				<dd class="dd">
					JSON</dd>
				<dt class="dt">
					<a name=""><!-- --></a>HTTP Method</dt>
				<dd class="dd">
					GET</dd>
				<dt class="dt">
					<a name=""><!-- --></a>Authentication</dt>
				<dd class="dd">
					<samp class="codeph nolang">Authorization: Basic Auth token:secret</samp></dd>
				<dt class="dt">
					<a name=""><!-- --></a>URL Query Parameters</dt>
				<dd class="dd">None</dd>
				<dt class="dt">
					<a name="order_result_fields"><!-- --></a>Return Result</dt>
				<dd class="dd">
				The result is represented as a JSON string:
		<table border="0" cellpadding="4" cellspacing="0" class="featureTable" dir="ltr" id="tblMain">
			<thead align="left" class="thead">
				<tr class="row">
					<th class="featureTableHeader" width="200">
						Fields in Result</th>
					<th class="featureTableHeader">
						Description</th>
				</tr>
			</thead>
			<tbody>
<?php
	$description = array(
		'LocationQuoteID'	=> 'Location quote id',
		'ProviderName'		=> "Provider's name",
		'TotalMRC'			=> 'Grand total of monthly recurring charge',
		'TotalNRC'			=> 'Grand total of non-recurring charge',
		'LoopMRC'			=> 'Sub total of monthly recurring charge for Local Access',
		'LoopNRC'			=> 'Sub total of non-recurring charge for Local Access',
		'PortMRC'			=> 'Sub total of monthly recurring charge for Port',
		'PortNRC'			=> 'Sub total of non-recurring charge for Port',
		'RouterMRC'			=> 'Sub total of monthly recurring charge for Router',
		'RouterNRC'			=> 'Sub total of non-recurring charge Router',
		'QoSMRC'			=> 'Sub total of monthly recurring charge for QoS',
		'QosNRC'			=> 'Sub total of non-recurring charge for QoS',
		'ServiceMRC'		=> 'Sub total of monthly recurring charge for Service',
		'ServiceNRC'		=> 'Sub total of non-recurring charge for Service',
	);

	foreach ($description as $field => $desc) {
		echo '<tr dir="ltr"><td class="s10" dir="ltr">'.$field.'</td><td class="s7">'.$desc.'</td></tr>' . PHP_EOL;
	}
?>
			</tbody>
		</table>
		<br>Example output:
<pre>
{

    "LocationQuoteId":21111,
    "ProviderName":"TelePacific",
    "TotalMRC":"750.000000",
    "TotalNRC":"750.000000",
    "LoopMRC":"0",
    "LoopNRC":"0",
    "PortMRC":"0",
    "PortNRC":"0",
    "RouterMRC":"0.000000",
    "RouterNRC":"0",
    "QoSMRC":"0",
    "QoSNRC":"250.000000",
    "ServiceMRC":"750.000000",
    "ServiceNRC":"500.000000"

}
</pre>
		</dd>
			</dl>
		</div>



        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
