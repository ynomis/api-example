<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>MSS REST API Documentation | Agent</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>

        <link rel="stylesheet" href="css/help.css">
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
		<h1 class="helpHead1">
			<a name="heading_1_1"><!-- --></a><span class="ph" id="topic-title"><a name="topic-title"><!-- --></a>Agent by agent id</span></h1>
		<div class="body">
			<p class="p">
				Get a agent info.</p>
			<dl class="dl">
				<dt class="dt">
					<a name=""><!-- --></a>URI</dt>
				<dd class="dd">
					<samp class="codeph nolang"><samp class="codeph nolang"><samp class="codeph nolang"><samp class="codeph nolang">/agent/</samp>&lt;agentid&gt;</samp></samp></samp></dd>
				<dt class="dt">
					<a name=""><!-- --></a>Result Formats</dt>
				<dd class="dd">
					JSON</dd>
				<dt class="dt">
					<a name=""><!-- --></a>HTTP Method</dt>
				<dd class="dd">
					GET</dd>
				<dt class="dt">
					<a name=""><!-- --></a>Authentication</dt>
				<dd class="dd">
					<samp class="codeph nolang">Authorization: Basic Auth token:secret</samp></dd>
				<dt class="dt">
					<a name=""><!-- --></a>URL Query Parameters</dt>
				<dd class="dd">None</dd>
				<dt class="dt">
					<a name="order_result_fields"><!-- --></a>Return Result</dt>
				<dd class="dd">
				The result is represented as a JSON string:
		<table border="0" cellpadding="4" cellspacing="0" class="featureTable" dir="ltr" id="tblMain">
			<thead align="left" class="thead">
				<tr class="row">
					<th class="featureTableHeader" width="200">
						Fields in Result</th>
					<th class="featureTableHeader">
						Description</th>
				</tr>
			</thead>
			<tbody>
<?php
	$description = array(
        'id'                            => 'Unique agent id',
        'ma_id'                         => 'Master agent id',
        'agent_company_id'              => 'Agent company id',
        'promo_code'                    => '4 Character Promo Code',
        'reports_to'                    => '',
        'privileges'                    => '',
        'type'                          => '',
        'firstname'                     => '',
        'lastname'                      => '',
        'title'                         => '',
        'address'                       => '',
        'city'                          => '',
        'state'                         => '',
        'zip'                           => '',
        'phone'                         => 'Format: DDD-DDD-DDDD, e.g. 213-123-1234.',
        'fax'                           => '',
        'extension'                     => '',
        'mobile'                        => '',
        'email'                         => '',
        'setup_date'                    => '',
        'last_updated'                  => '',
        'inactive'                      => '1 - true, client is not active <br /> 0 - false, client is active',
        'inactive_date'                 => 'E.g. 2006-05-13T00:00:00-0700',
        'comments'                      => '',
        'quote_ready_email_1st_message' => '',
        'quote_ready_email_2nd_message' => '',
        'quote_ready_email_salutation'  => '',
        'quote_ready_email_signoff_as'  => '',
        'custom_sidebar_links'          => '',
        'custom_agent_field_value'      => '',
        'comm_view'                     => '',
        'show_bulletin_board'           => '',
        'show_document_library'         => '',
        'country'                       => '',
        'acceptance_terms_of_use'       => 'Acceptance terms of use',
        'acceptance'                    => '',
        'landing_page'                  => '',
        'channel_manager_id'            => '',
        'edit_channel_manager'          => '',
        'attribute_1'                   => 'Customer attribute 1',
        'attribute_2'                   => 'Customer attribute 2',
        'attribute_3'                   => 'Customer attribute 3',
	);

	require_once('../autoload.php');
	foreach (\RESTAPI\AgentsFacade::$agentDataTypes as $field => $column) {
		if ($column['read']) {
			//$required = ($column['required']) ? '<span class="required">*</span>' : '';
			echo '<tr dir="ltr"><td class="s10" dir="ltr">'.$column['alias'].'</td><td class="s7">'.$description[$field].'</td></tr>' . PHP_EOL;
		}
	}
?>
			</tbody>
		</table>
		<br>
		Example output:
<pre>
{

    "AgentId":193,
    "AssociateId":1,
    "AgentCompanyId":127,
    "PromoCode":null,
    "ReportsTo":null,
    "AgentType":"Agent Administrator",
    "FirstName":"John",
    "LastName":"Doe",
    "Title":"CEO",
    "Address":null,
    "City":null,
    "State":null,
    "Zip":null,
    "Phone":"213-999-9999",
    "Fax":null,
    "Extension":null,
    "Mobile":null,
    "Email":"john.doe@salstreamsoft.com",
    "SetupDate":"2008-09-30T00:00:00-0700",
    "LastUpdated":"2008-09-30T00:00:00-0700",
    "Inactive":"1",
    "InactiveDate":null,
    "Comments":null,
    "Country":null,
    "AcceptedTou":"1",
    "Acceptance":null,
    "Attribute1":null,
    "Attribute2":null,
    "Attribute3":null

}
</pre>
		</dd>
			</dl>
		</div>



        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
