<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>MSS REST API Documentation | Client Company By Name</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>

        <link rel="stylesheet" href="css/help.css">
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
		<h1 class="helpHead1">
			<a name="heading_1_1"><!-- --></a><span class="ph" id="topic-title"><a name="topic-title"><!-- --></a>Client Company By Name</span></h1>
		<div class="body">
			<p class="p">
				Get a client company object by its properties. Please check following Query Paramters section for supported keys</p>
			<dl class="dl">
				<dt class="dt">
					<a name=""><!-- --></a>URI</dt>
				<dd class="dd">
					<samp class="codeph nolang"><samp class="codeph nolang"><samp class="codeph nolang"><samp class="codeph nolang">/customer</samp></samp></samp></dd>
				<dt class="dt">
					<a name=""><!-- --></a>Result Formats</dt>
				<dd class="dd">
					JSON</dd>
				<dt class="dt">
					<a name=""><!-- --></a>HTTP Method</dt>
				<dd class="dd">
					GET</dd>
				<dt class="dt">
					<a name=""><!-- --></a>Authentication</dt>
				<dd class="dd">
					<samp class="codeph nolang">Authorization: Basic Auth token:secret</samp></dd>
				<dt class="dt">
					<a name=""><!-- --></a>URL Query Parameters</dt>
				<dd class="dd">
					Unsupported keys will be ignored.
					<table cellpadding="4" cellspacing="0" class="featureTable" summary="">
						<thead align="left" class="thead">
							<tr class="row">
								<th class="featureTableHeader" width="20%">
									Keys</th>
								<th class="featureTableHeader" >
									Required?</th>
								<th class="featureTableHeader" id="d39497e115" >
									Description</th>
							</tr>
						</thead>
						<tbody class="tbody">
							<tr>
								<td class="entry"  >
									companyName</td>
								<td class="entry"  >
									Yes</td>
								<td class="entry"  >
								Case-insensetive. This is not a partial match, although special charecters are ignored. Please refer to following regular expression for matching rules, /[^0-9a-zA-Z& ]/<br />
								Query key is case-sensetive.</td>
							</tr>
						</tbody>
					</table>
				</dd>
				<dt class="dt">
					<a name="order_result_fields"><!-- --></a>Return Result</dt>
				<dd class="dd">
				The result is the found client company presented as a JSON object. If not found, an error will be returned in JSON. A client company object may look like:
		<table border="0" cellpadding="4" cellspacing="0" class="featureTable" dir="ltr" id="tblMain">
			<thead align="left" class="thead">
				<tr class="row">
					<th class="featureTableHeader" width="200">
						Fields in Result</th>
					<th class="featureTableHeader">
						Description</th>
				</tr>
			</thead>
			<tbody>
<?php
	$description = array(
		'agent_id'		=> 'Client Company\'s agent id',
		'general_phone'	=> 'Format: DDD-DDD-DDDD, e.g. 213-123-1234.',
		'general_fax'	=> 'Format: DDD-DDD-DDDD, e.g. 213-123-1234.',
		'signup_date'	=> 'E.g. 2006-05-13T00:00:00-0700',
		'inactive'		=> '1 - true, client is not active <br /> 0 - false, client is active',
		'inactive_date'	=> 'E.g. 2006-05-13T00:00:00-0700',
		'lead'			=> '1 - true, is a lead <br /> 0 - false, is not a lead',
		'preferred_contact_method'	=> 'E.g. Email, Work Phone, etc.',
	);

	require_once('../autoload.php');
	foreach (\RESTAPI\ClientCompaniesFacade::$clientCompanyDataTypes as $field => $column) {
		if ($column['read']) {
			//$required = ($column['required']) ? '<span class="required">*</span>' : '';
			echo '<tr dir="ltr"><td class="s10" dir="ltr">'.$column['alias'].'</td><td class="s7">'.$description[$field].'</td></tr>' . PHP_EOL;
		}
	}
?>
			</tbody>
		</table>
		<br>
		Output format will be the same as "<a href="client_company_by_id.php">Client Company by Id</a>" endpoint, click <a href="client_company_by_id.php">here</a> for detail.
		</dd>
			</dl>
		</div>
&nbsp;


        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
