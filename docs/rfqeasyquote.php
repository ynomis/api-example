<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>MSS REST API Documentation | Request for Quotes - EasyQuote</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/main.css">
	<script src="js/vendor/modernizr-2.6.2.min.js"></script>

	<link rel="stylesheet" href="css/help.css">
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

<!-- Add your site or application content here -->
<h1 class="helpHead1">
	<a name="heading_1_1"><!-- --></a><span class="ph" id="topic-title"><a name="topic-title"><!-- --></a>Request for Quotes - EasyQuote</span></h1>

<div class="body">
	<p class="p">
		Creates a request for quotes, implementing EasyQuote request format</p>
	<dl class="dl">

		<dt class="dt">
			<a name=""><!-- --></a>URI
		</dt>
		<dd class="dd">
			<samp class="codeph nolang"><samp class="codeph nolang"><samp class="codeph nolang"><samp class="codeph nolang">/rfqeasyquote</samp></dd>
		<dt class="dt">
			<a name=""><!-- --></a>Result formats
		</dt>
		<dd class="dd">
			JSON
		</dd>
		<dt class="dt">
			<a name=""><!-- --></a>HTTP Method
		</dt>
		<dd class="dd">
			POST
		</dd>
		<dt class="dt">
			<a name=""><!-- --></a>Authentication
		</dt>
		<dd class="dd">
			<samp class="codeph nolang">Authorization: Basic Auth token:secret</samp></dd>
		<dt class="dt">
			<a name=""><!-- --></a>URL Query Parameters
		</dt>
		<dd class="dd">None</dd>
		<dt class="dt">
			<a name=""><!-- --></a>POST Data
		</dt>
		<dd class="dd">
			<table cellpadding="4" cellspacing="0" class="featureTable" summary="">
				<thead align="left" class="thead">
				<tr class="row">
					<th class="featureTableHeader" width="20%">
						Fields
					</th>
					<th class="featureTableHeader">
						Required?
					</th>
					<th class="featureTableHeader" id="d39497e115">
						Description
					</th>
					<th class="featureTableHeader">
						Example
					</th>
				</tr>
				</thead>
				<tbody class="tbody">
				<tr class="row">
					<td class="entry">customerInfo</td>
					<td class="entry">Yes</td>
					<td class="entry">
<pre>
[{
	"contact":"test name",
	"phone":"5555555555",
	"email":"test@ssssoft.com",
	"company":"Test Company 992"
}]
</pre>
					</td>
					<td class="entry">&nbsp;</td>
				</tr>
				<tr class="row">
					<td class="entry">productType</td>
					<td class="entry">Yes</td>
					<td class="entry">Text value</td>
					<td class="entry">Internet</td>
				</tr>
				<tr class="row">
					<td class="entry">config</td>
					<td class="entry">Yes</td>
					<td class="entry">Config values are different from one Mss account to another. The complete list of acceptable config values for an account can be provided by the administrator of the Mss account you plan to call.</td>
					<td class="entry">DS-1/1.5M</td>
				</tr>
				<tr class="row">
					<td class="entry">terms</td>
					<td class="entry">Yes</td>
					<td class="entry">Integer value or comma delimited integers</td>
					<td class="entry">1,2,3</td>
				</tr>
				<tr class="row">
					<td class="entry">locations</td>
					<td class="entry">Yes</td>
					<td class="entry">
<pre>
[{
	{
	"npanxx":"5555555",
	"street":"555 El Dorado",
	"city":"Broomfield",
	"state":"CO",
	"zip":"80021",
	"npanxx_z":"555556",
	"street_z":"555 Broadway",
	"city_z":"New York",
	"state_z":"NY",
	"zip_z":"10010"
	},
	{...}
}]
</pre>
					</td>
					<td class="entry">&nbsp;</td>
				</tr>
				</tbody>
			</table>
		</dd>

		<dt class="dt">
			<a name="order_result_fields"><!-- --></a>Return Result
		</dt>
		<dd class="dd">
			The result is represented as a JSON string:
<pre>
{
	"rfqTrackingNumber":"5286320886",
	"callsRemaining":""
}
</pre>
		</dd>


	</dl>
</div>


</body>
</html>
