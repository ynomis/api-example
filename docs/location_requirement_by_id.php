<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>MSS REST API Documentation | Location Requirement By Id</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>

        <link rel="stylesheet" href="css/help.css">
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
		<h1 class="helpHead1">
			<a name="heading_1_1"><!-- --></a><span class="ph" id="topic-title"><a name="topic-title"><!-- --></a>Location Requirement By Id</span></h1>
		<div class="body">
			<p class="p">
				Get location requirement by its id.</p>
			<dl class="dl">
				<dt class="dt">
					<a name=""><!-- --></a>URI</dt>
				<dd class="dd">
					<samp class="codeph nolang"><samp class="codeph nolang"><samp class="codeph nolang"><samp class="codeph nolang">/locrequirement/</samp>&lt;location_reqirement_id&gt;</samp></samp></samp></dd>
				<dt class="dt">
					<a name=""><!-- --></a>Result Formats</dt>
				<dd class="dd">
					JSON</dd>
				<dt class="dt">
					<a name=""><!-- --></a>HTTP Method</dt>
				<dd class="dd">
					GET</dd>
				<dt class="dt">
					<a name=""><!-- --></a>Authentication</dt>
				<dd class="dd">
					<samp class="codeph nolang">Authorization: Basic Auth token:secret</samp></dd>
				<dt class="dt">
					<a name=""><!-- --></a>URL Query Parameters</dt>
				<dd class="dd">None</dd>
				<dt class="dt">
					<a name="order_result_fields"><!-- --></a>Return Result</dt>
				<dd class="dd">
				The result is represented as a JSON string:
		<table border="0" cellpadding="4" cellspacing="0" class="featureTable" dir="ltr" id="tblMain">
			<thead align="left" class="thead">
				<tr class="row">
					<th class="featureTableHeader" width="200">
						Fields in Result</th>
					<th class="featureTableHeader">
						Description</th>
				</tr>
			</thead>
			<tbody>
<?php
	$description = array(
		'custom_fields' => 'Custom fields that are defined within your company. Object key in this section is the custom field id.',
		'dynamic_fields' => 'Dynamic fields that defined within your company. If not at all, this section won\'t show. Object key in this section is the custom field id.',
	);

	require_once('../autoload.php');
	foreach (\RESTAPI\RfqsFacade::$locRequirementDataTypes as $field => $column) {
		if ($column['read']) {
			//$required = ($column['required']) ? '<span class="required">*</span>' : '';
			echo '<tr dir="ltr"><td class="s10" dir="ltr">'.$column['alias'].'</td><td class="s7">'.$description[$field].'</td></tr>' . PHP_EOL;
		}
	}
	echo '<tr dir="ltr"><td class="s10" dir="ltr">CustomFields</td><td class="s7">'.$description['custom_fields'].'</td></tr>' . PHP_EOL;
	echo '<tr dir="ltr"><td class="s10" dir="ltr">DynamicFields</td><td class="s7">'.$description['dynamic_fields'].'</td></tr>' . PHP_EOL;
?>
			</tbody>
		</table>
		<br>Example output:
<pre>
{

    "LocationRequirementId":66666,
    "ParentRequirementId":55555,
    "LocationName":"Loc 1",
    "NPANXX":123456,
    "Address":"1234 country rd",
    "City":"bensenville",
    "State":"IL",
    "ZIP":"60106",
    "LocationName2":null,
    "NPANXX2":null,
    "Address2":null,
    "City2":null,
    "State2":null,
    "ZIP2":null,
    "ServiceStatus":"Ready",
    "CustomFields":{
        "1":{
            "label":"Select a Service Type",
            "short":"Service Type",
            "value":"Mitel Mobile"
        },
        "2":{
        	...
        },

        ...

    },
    "DynamicFields":{
        "110":{
            "label":"Franklin U771 3G\/4G USB Modem",
            "value":"1"
        },
        "111":{
            ...
        },
        
        ...
        
    }

}
</pre>
		</dd>
			</dl>
		</div>



        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
