<?php

/**
 *
 * This is to avoid hitting production Database from reading docs on live site.
 * ALl rest of sub domains will use their own domain settings
 * may need crossDomain flag in AJAX calls.
 *
 * syang,
 *
 */
$subDomain = str_replace('.ssssoft.com', '', $_SERVER["HTTP_HOST"]);

$api_sandbox = ($subDomain == 'mam') ? 'mam1' : '';

