<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>MSS REST API Documentation | Update Order Status</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>

        <link rel="stylesheet" href="css/help.css">
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
		<h1 class="helpHead1">
			<a name="heading_1_1"><!-- --></a><span class="ph" id="topic-title"><a name="topic-title"><!-- --></a>Update Order Status</span></h1>
		<div class="body">
			<p class="p">
				Update an order's status. Please reference following "Post Data" for supported statuses, unsupported status value will cause request error. Order update via API should be
				enabled for your company before it is working. Once a company takes status update from API, it will no longer be able to update order status through MSS web
				interface, nor the benchmark date tracking. To activate this feature, please consult your account manager.</p>
			<dl class="dl">

				<dt class="dt">
					<a name=""><!-- --></a>URI</dt>
				<dd class="dd">
					<samp class="codeph nolang"><samp class="codeph nolang"><samp class="codeph nolang"><samp class="codeph nolang">/order/</samp>&lt;orderid&gt;/updatestatus</samp></samp></dd>
				<dt class="dt">
					<a name=""><!-- --></a>Result formats</dt>
				<dd class="dd">
					JSON</dd>
				<dt class="dt">
					<a name=""><!-- --></a>HTTP Method</dt>
				<dd class="dd">
					PUT</dd>
				<dt class="dt">
					<a name=""><!-- --></a>Authentication</dt>
				<dd class="dd">
					<samp class="codeph nolang">Authorization: Basic Auth token:secret</samp></dd>
				<dt class="dt">
					<a name=""><!-- --></a>URL Query Parameters</dt>
				<dd class="dd">None</dd>
				<dt class="dt">
					<a name=""><!-- --></a>POST Data</dt>
				<dd class="dd">
					<table cellpadding="4" cellspacing="0" class="featureTable" summary="">
						<thead align="left" class="thead">
							<tr class="row">
								<th class="featureTableHeader" width="20%">
									Keys</th>
								<th class="featureTableHeader" >
									Required?</th>
								<th class="featureTableHeader" id="d39497e115" >
									Description</th>
							</tr>
						</thead>
						<tbody class="tbody">
							<tr>
								<td class="entry"  >
									newStatus</td>
								<td class="entry"  >
									Yes</td>
								<td class="entry"  >
									Supported values:<br />
<?php
	require_once('../autoload.php');
	foreach (\RESTAPI\OrdersFacade::$validOrderStatusViaApiUpdate as $key => $v) {
		echo "&nbsp;&nbsp;&nbsp;&nbsp;$key<br />";
	}
?>
								</td>
							</tr>
							</tbody>
					</table>
				</dd>

				<dt class="dt">
					<a name="order_result_fields"><!-- --></a>Return Result</dt>
				<dd class="dd">
				If successful, a success message will return, in HTTP code of 200. In errors, an error message in JSON format will return, in HTTP code of 4xx.
				</dd>

				<dt class="dt">Tester</dt>
				<dd>
<style>
.tester td {padding: 3px; border: 1px solid #ccc; }
.tester input, .tester select {margin: 0 10px; width: 250px;}
.tester {width: 75%; }
.tester .cleft {width: 200px;}
.tester .required {color: red; font-weight: bold; }
</style>
<table class="featureTable tester" dir="ltr">
	<!--tr><td class="cleft">API Token: <span class="required">*</span></td><td><input id="token" value="" /></td></tr>
	<tr><td>API Secret: <span class="required">*</span></td><td><input id="secret" value="" /></td></tr-->
	<tr><td>Order Id: <span class="required">*</span></td><td><input id="id" name="id" value="" /></td></tr>
	<tr><td>New Order Status: <span class="required">*</span></td>
		<td><select id="newStatus" name="newStatus" value="">
<?php
	require_once('../autoload.php');
	foreach (\RESTAPI\OrdersFacade::$validOrderStatusViaApiUpdate as $status => $def) {
		echo "<option value='{$status}'>{$status}</option>" . PHP_EOL;
	}
?>
		</select></td></tr>
	<tr><td colspan="2"><button id="apisubmit">Submit</button></td></tr>
	<tr><td colspan="2" id="ajaxresp" style="color: green;"></td></tr>
</table>
				</dd>

			</dl>
		</div>



        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Action Tester -->
<?php require_once 'api_host.php'; ?>
        <script>
		var host = '<?php echo $api_sandbox; ?>';

        $(document).ready(function () {
			$('#apisubmit').click(function($e) {
				var order_id = $('#id').val();
				if (!parseInt(order_id)) {
					alert('Order id is empty or invalid.');
					return false;
				}

				var _data = {newStatus : $('#newStatus').val()};

				$.ajax({
					type:	'PUT',
					url:	host + '/api.v2/rest/order/' + order_id + '/updatestatus',
					dataType:	'json',
					data:	_data,
					success: function(data) {
						$('#ajaxresp').html('<p>Response:</p><pre>' + JSON.stringify(data) + '</pre>');
					},
					error: function (data) {
						console.log(data);
						$('#ajaxresp').html('<p>Response:</p>{' + data.status + ': ' +data.statusText + '}<br /><pre>' +data.responseText + '</pre>');
					}
				});
			});
		});
		</script>

    </body>
</html>
