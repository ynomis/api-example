<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>MSS REST API Documentation | Order Payments by Date Range</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>

        <link rel="stylesheet" href="css/help.css">
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
		<h1 class="helpHead1">
			<a name="heading_1_1"><!-- --></a><span class="ph" id="topic-title"><a name="topic-title"><!-- --></a>Order Payments by Date Range</span></h1>
		<div class="body">
			<p class="p">
				Get order id with its docusign status (if available), and credit card payment info (if available). startDate and endDate are all inclusive. User may also filter on client company by providing client company id, see Query Parameters for detail.</p>
			<dl class="dl">
				<dt class="dt">
					<a name=""><!-- --></a>URI</dt>
				<dd class="dd">
					<samp class="codeph nolang"><samp class="codeph nolang"><samp class="codeph nolang"><samp class="codeph nolang">/orderpayments</samp></samp></samp></dd>
				<dt class="dt">
					<a name=""><!-- --></a>Result Formats</dt>
				<dd class="dd">
					JSON</dd>
				<dt class="dt">
					<a name=""><!-- --></a>HTTP Method</dt>
				<dd class="dd">
					GET</dd>
				<dt class="dt">
					<a name=""><!-- --></a>Authentication</dt>
				<dd class="dd">
					<samp class="codeph nolang">Authorization: Basic Auth token:secret</samp></dd>
				<dt class="dt">
					<a name=""><!-- --></a>URL Query Parameters</dt>
				<dd class="dd">
					<table cellpadding="4" cellspacing="0" class="featureTable" summary="">
						<thead align="left" class="thead">
							<tr class="row">
								<th class="featureTableHeader" width="20%">
									Filters</th>
								<th class="featureTableHeader" >
									Required?</th>
								<th class="featureTableHeader" id="d39497e115" >
									Description</th>
							</tr>
						</thead>
						<tbody class="tbody">
							<tr>
								<td class="entry"  >
									startDate</td>
								<td class="entry"  >
									Yes</td>
								<td class="entry"  >
									Date format is: YYYY-MM-DD</td>
							</tr>
							<tr>
								<td class="entry"  >
									endDate</td>
								<td class="entry"  >
									Yes</td>
								<td class="entry"  >
									Date format is: YYYY-MM-DD</td>
							</tr>
							<tr>
								<td class="entry"  >
									paymentMethod</td>
								<td class="entry"  >
									No</td>
								<td class="entry"  >
									Payment Method. Custom defined value through order interview questions</td>
							</tr>
							<tr>
								<td class="entry"  >
									paymentStatus</td>
								<td class="entry"  >
									No</td>
								<td class="entry"  >
									Payment Status. Custom defined value through order interview questions</td>
							</tr>
							<tr>
								<td class="entry"  >
									docusignStatus</td>
								<td class="entry"  >
									No</td>
								<td class="entry"  >
									DocuSign Status. accepted values:<br />
'sent',<br />
'created',<br />
'voided',<br />
'deleted',<br />
'delivered',<br />
'signed',<br />
'completed',<br />
'declined',<br />
'timedout',<br />
'processing',<br />
								</td>
							</tr>
							</tbody>
					</table>
				</dd>
				<dt class="dt">
					<a name="order_result_fields"><!-- --></a>Return Result</dt>
				<dd class="dd">
				The result is represented as a JSON string. Sample output:
				<pre>
[

    {
        "orderId":1234,
        "orderDate":"2013-05-14",
        "paymentMethod":"Credit Card",
        "paymentStatus":"Pending"
        "docusignStatus":"sent",
    },
    {
        "orderId":1235,
        "orderDate":"2013-04-24",
        "paymentMethod":"Credit Card",
        "paymentStatus":"Complete"
        "docusignStatus":"completed",
    }

]
				</pre>
				</dd>
			</dl>
		</div>



        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
