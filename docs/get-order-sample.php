<?php

$token		= ''; // Your company API token for data
$secret		= ''; // Your company API token secret
$agentid	= 0;  // the agent id to be queried against
$host		= 'http://devvm.ssssoft.com';
$path		= '/api.v2/rest/agent/' . $agentid . '/orders';
$url		= $host . $path;

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// send the username and password
curl_setopt($ch, CURLOPT_USERPWD, $token . ':' . $secret);

$output = curl_exec($ch);
curl_close($ch);

echo $output;
