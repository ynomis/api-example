<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>MSS REST API Documentation | Quotes with SKU</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>

        <link rel="stylesheet" href="css/help.css">
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
		<h1 class="helpHead1">
			<a name="heading_1_1"><!-- --></a><span class="ph" id="topic-title"><a name="topic-title"><!-- --></a>Get Quote Details with SKUs</span></h1>
		<div class="body">
			<p class="p">
				Use the GET method to retrieve quote details of location with SKUs by given date range.</p>
			<dl class="dl">
				<dt class="dt">
					<a name=""><!-- --></a>URI</dt>
				<dd class="dd">
					<samp class="codeph nolang"><samp class="codeph nolang"><samp class="codeph nolang"><samp class="codeph nolang">/quotesku</samp></samp></samp></samp></dd>
				<dt class="dt">
					<a name=""><!-- --></a>Result formats</dt>
				<dd class="dd">
					JSON</dd>
				<dt class="dt">
					<a name=""><!-- --></a>HTTP Method</dt>
				<dd class="dd">
					GET</dd>
				<dt class="dt">
					<a name=""><!-- --></a>Authentication</dt>
				<dd class="dd">
					<samp class="codeph nolang">Authorization: Basic Auth token:secret</samp></dd>
				<dt class="dt">
					<a name=""><!-- --></a>URL Query Parameters</dt>
				<dd class="dd">
					<table cellpadding="4" cellspacing="0" class="featureTable" summary="">
						<thead align="left" class="thead">
							<tr class="row">
								<th class="featureTableHeader" width="20%">
									Filters</th>
								<th class="featureTableHeader" >
									Required?</th>
								<th class="featureTableHeader" id="d39497e115" >
									Description</th>
							</tr>
						</thead>
						<tbody class="tbody">
							<tr>
								<td class="entry"  >
									startDate</td>
								<td class="entry"  >
									Yes</td>
								<td class="entry"  >
									Date format is: YYYY-MM-DD</td>
							</tr>
							<tr>
								<td class="entry"  >
									endDate</td>
								<td class="entry"  >
									Yes</td>
								<td class="entry"  >
									Date format is: YYYY-MM-DD</td>
							</tr>
						</tbody>
					</table>
				</dd>
				<dt class="dt">
					<a name="order_result_fields"><!-- --></a>Return Result</dt>
				<dd class="dd">
				The result is represented as a JSON string. For example,
				<pre>
[{
	"LocationQuoteId" : 1234567,
	"ParentQuoteId"   : 2345678,
	"RequestId"       : 34567,
	"ApiQuoteId"      : "RFQ# 1234567890",
	"QuoteComplete"   : true,
	"LastUpdated"     : null,
	"QuoteComments"   : "\nEthernet over Copper i...rmed at time of order.\n",
	"ShownOnProposal" : false,
	"SKU"             : [{
		"Name" : "Ethernet- 3MB EoC",
		"Quantity" : 1,
		"AmountType" : "MRC",
		"Amount" : "234.770000",
		...
	},

	{...}

	]
},

{...}

]
				</pre>
		<strong>Supported Fields in Quote Detail:</strong>
		<table border="0" cellpadding="4" cellspacing="0" class="featureTable" dir="ltr" id="tblMain">
			<thead align="left" class="thead">
				<tr class="row">
					<th class="featureTableHeader" width="200">
						Fields in Result</th>
					<th class="featureTableHeader">
						Description</th>
				</tr>
			</thead>
			<tbody>
<?php
	$description = array(
		'location_id'			=> 'Location Quote Indentifier',
		'loc_quote_complete'	=> '1 - true, quote is complete <br /> 0 - false, quote is not complete',
		'show'					=> '1 - true, is shown <br /> 0 - false, is not shown',
	);

	require_once('../autoload.php');
	foreach (\RESTAPI\OrdersFacade::$quotePerLocationSpecs as $field => $column) {
		echo '<tr dir="ltr"><td class="s10" dir="ltr">'.$column.'</td><td class="s7">'.$description[$field].'</td></tr>' . PHP_EOL;
	}
?>
			</tbody>
		</table>

		<br /><strong>Supported Fields in SKU object:</strong>
		<table border="0" cellpadding="4" cellspacing="0" class="featureTable" dir="ltr" id="tblMain">
			<thead align="left" class="thead">
				<tr class="row">
					<th class="featureTableHeader" width="200">
						Fields in Result</th>
					<th class="featureTableHeader">
						Description</th>
				</tr>
			</thead>
			<tbody>
<?php
	$description = array(
		'name'		=> 'SKU Name',
		'amount'	=> 'Float with decimal point',
		'markup'	=> 'Float with decimal point',
		'discount'	=> 'Float with decimal point',
		'discount_applied'	=> 'Float with decimal point',
	);

	require_once('../autoload.php');
	foreach (\RESTAPI\OrdersFacade::$quoteSkuSpecs as $field => $column) {
		echo '<tr dir="ltr"><td class="s10" dir="ltr">'.$column.'</td><td class="s7">'.$description[$field].'</td></tr>' . PHP_EOL;
	}
?>
			</tbody>
		</table>

				</dd>
			</dl>
		</div>



        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
