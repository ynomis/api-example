<?php

/**
 * Require this file should autoload all classes in RESTAPI namespace.
 * And all those classes are lazy-loading in general.
 *
 */

spl_autoload_register(function($classname) {
	if(strpos($classname, 'RESTAPI') !== false) { // only auto load RESTAPI classes
		$paths = explode('\\', $classname);
		$filename = __DIR__ . '/' . implode(DIRECTORY_SEPARATOR, $paths) . '.php';
		if (file_exists($filename)) {
			include_once($filename);
		} else {
			error_log('Autoload class failed: ' . $filename);
		}
	}
});

