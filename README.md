# Project Summary #

This is a REST API project built based on Slim. It meant to follow most of common design principals of RESTful API. Such as:

* Use RESTful URLs and actions;

* Versioning through URL;

* Using SSL and Basic Auth for authentication;

* Forced JSON only responses;

* Autoloading model/facade classes in same namespace;

* Using Facade class, such as [ClientCompanyFacade](https://bitbucket.org/ynomis/api-example/src/649023f253cfba3a72770dfb2000d4b1aada8e04/RESTAPI/ClientCompaniesFacade.php?at=master), to separate concerns from controller (Slim endpoints).

* Using output filter to normalize dataset in response, thus make it configurable (turing programming to configuration);

* Detail documentation;