<?php
// ini_set('display_errors', 1);
// error_reporting(E_ALL);

define('AUTH_REALM', 'MssAPI');
define('APPLICATION_PREFIX', 'mastre.');

ini_set('max_execution_time', 900);
require DOC_ROOT . '/inc/Slim/Slim.php';
\Slim\Slim::registerAutoloader();

/**
 * Yes, the order related actions were developed before AR models are even
 * introduced. I had to use PDO for database access. Once Order models are
 * build, we can revisit these calls and refactor. But for new actions to
 * be added here, better build related AR models if they do not exist yet.
 */
require_once DOC_ROOT . "/inc/db/pdo.php";

// Autoload classes in RESTAPI namespace
require_once DOC_ROOT . "/api.v2/autoload.php";

// Autoload model classes in MSS namespace
require_once(DOC_ROOT . '/models/autoload.php');

//With default settings
$app = new \Slim\Slim(array(
	'debug' => false,
));
//With custom settings
/*
$app = new Slim(array(
    'log.enable' => true,
    'log.path' => './logs',
    'log.level' => 4
    //'view' => 'MyCustomViewClassName'
));
*/

// $app->add(new \Slim\Middleware\ContentTypes());

$app->hook('slim.before.router', function() use ($app) {
	$response = $app->response();
	if (isset($_SERVER['PHP_AUTH_USER']) && !empty($_SERVER['PHP_AUTH_USER']) &&
		isset($_SERVER['PHP_AUTH_PW']) && !empty($_SERVER['PHP_AUTH_PW'])) {
		$token	= $_SERVER['PHP_AUTH_USER'];
		$secret	= $_SERVER['PHP_AUTH_PW'];

		$auth = \RESTAPI\Auth::authenticate($token, $secret);
		if (!$auth) {
			$response->header('WWW-Authenticate', 'Basic realm="' . AUTH_REALM . '"');
			$app->halt(401, "Unauthorized use.");
		}
		\Slim\Environment::getInstance()->offsetSet(APPLICATION_PREFIX . 'macompanyid', $auth['ma_company_id']);
		\Slim\Environment::getInstance()->offsetSet(APPLICATION_PREFIX . 'token_type', $auth['token_type']);
		\Slim\Environment::getInstance()->offsetSet(APPLICATION_PREFIX . 'token', $token);
	} else {
		$response->header('WWW-Authenticate', 'Basic realm="' . AUTH_REALM . '"');
		$app->halt(401, "Unauthorized use.");
	}

});


$app->get('/customer/:customerid/orders', function($customerid) use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$facade = new \RESTAPI\OrdersFacade();

	$request = $app->request();
	try {
		$out = $facade->getOrdersByClientCompany($macompanyid, $customerid, $request);
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}

	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$response->body(json_encode($out));
});

/**
 * Get an order by order_id
 */
$app->get('/order/:orderid', function($orderid) use ($app) {

	$macompanyid = \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$facade = new \RESTAPI\OrdersFacade();

	try {
		$out = $facade->getOrder($macompanyid, $orderid);
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}

	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$response->body(json_encode($out));
});

/**
 * Get the orders for the company
 */
$app->get('/agent/:agentid/orders', function($agentid) use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$request = $app->request();

	$facade = new \RESTAPI\OrdersFacade();

	try {
		$out = $facade->getOrdersByAgent($macompanyid, $agentid, $request);
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}

	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$response->body(json_encode($out));
});

/**
 * Get order locations by order id
 */
$app->get('/order/:orderid/locations', function($orderid) use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$facade = new \RESTAPI\OrdersFacade();
	try {
		$out = $facade->getOrderLocations($macompanyid, $orderid);
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}

	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$response->body(json_encode($out));
});

/**
 * Get user uploaded order files by order id
 */
$app->get('/order/:orderid/files', function($orderid) use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$facade = new \RESTAPI\OrdersFacade();
	try {
		$zipfile = $facade->getOrderFiles($macompanyid, $orderid);
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}

	$response = $app->response();

    $response['Content-Description']		= 'File Transfer';
    $response['Content-Type']				= 'application/octet-stream';
    $response['Content-Disposition']		= 'attachment; filename=order_attachments_' . $orderid . '.zip';
    $response['Content-Transfer-Encoding']	= 'binary';
    $response['Expires']					= '0';
    $response['Cache-Control']				= 'must-revalidate';
    $response['Pragma']						= 'public';
    $response['Content-Length']				= filesize($zipfile);

    readfile($zipfile);
    unlink($zipfile);
});

/**
 * Get order interview q&a by order id
 */
$app->get('/orderinteviews/:orderid', function($orderid) use ($app) {

	$macompanyid = \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$facade = new \RESTAPI\OrdersFacade();

	try {
		$out = $facade->getOrderInterviewsByOrderId($macompanyid, $orderid);
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}

	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$response->body(json_encode($out));
});

/**
 * Get order interview q&a by quote id (quote_sq_id)
 */
$app->get('/orderinteviewsbyquote/:quoteid', function($quoteid) use ($app) {

	$macompanyid = \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$facade = new \RESTAPI\OrdersFacade();

	try {
		$out = $facade->getOrderInterviewsByQuoteId($macompanyid, $quoteid);
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}

	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$response->body(json_encode($out));
});

$app->get('/orderidsbydates', function() use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$facade = new \RESTAPI\OrdersFacade();

	try {
		$out = $facade->getOrderIdsByDates($macompanyid, $app->request());
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}

	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$response->body(json_encode($out));
});

$app->put('/order/:orderid/updatestatus', function($orderid) use ($app) {
	
	$key = 'newStatus';
	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	if (!$app->request()->put($key)) {
		$app->halt(400, 'No new status provided.');
	}

	$facade = new \RESTAPI\OrdersFacade();

	try {
		$out = $facade->updateOrderStatus($macompanyid, $orderid, $app->request()->put($key));
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}
	
	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);
	
	$response->body(json_encode($out));
	
});

$app->get('/order/:orderid/clientid', function($orderid) use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$facade = new \RESTAPI\OrdersFacade();
	try {
		$out = $facade->getClientIdByOrder($macompanyid, $orderid);
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	} catch (\Exception $e) {
		error_log($e->getMessage());
		$app->halt($e->getCode(), "{'error' : 'Unknown'}");
	}

	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$response->body(json_encode($out));

});

/**
 * Non-published endpoint, used by ANPI only now
 */
$app->put('/order/:orderid/updatepaymentstatus', function($orderid) use ($app) {

	$anpi_maid = 157;
	$key = 'newStatus';
	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');
	if ($anpi_maid != $macompanyid) {
		$app->halt(400, 'Error.');
	}

	if (!$app->request()->put($key)) {
		$app->halt(400, 'No new status provided.');
	}

	$facade = new \RESTAPI\OrdersFacade();

	try {
		$out = $facade->updateOrderPaymentStatus($macompanyid, $orderid, $app->request()->put($key));
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}

	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$response->body(json_encode($out));

});

/**
 * Non-published endpoint, used by ANPI only now.
 */
$app->get('/orderpayments', function() use ($app) {
	
	$anpi_maid = 157;
	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');
	if ($anpi_maid != $macompanyid) {
		$app->halt(400, 'Error.');
	}

	$request = $app->request();
	
	$facade = new \RESTAPI\OrdersFacade();
	
	try {
		$out = $facade->getOrderPaymentsByDates($macompanyid, $request);
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}
	
	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);
	
	$response->body(json_encode($out));

});

/* **** Below are client actions **** */

/**
 * Get client contact info
 */
$app->get('/client/:clientid', function ($clientid) use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$facade = new \RESTAPI\ClientsFacade();
	try {
		$out = $facade->getClient($macompanyid, $clientid);
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}

	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$response->body($out);

});

$app->put('/client/:clientid', function ($clientid) use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$facade = new \RESTAPI\ClientsFacade();
	try {
		$out = $facade->updateClient($macompanyid, $clientid, $app->request());
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}

	$response	= $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$response->body(json_encode($out));

});

$app->get('/customer/:customerid/client', function($customerid) use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');
	
	$facade = new \RESTAPI\ClientsFacade();
	try {
		$out = $facade->findClient($macompanyid, $customerid, $app->request());
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	} catch (\Exception $e) { // I probably should ask Slim to handle all uncatched Exception
		error_log($e->getMessage());
		$app->halt(500, "{'error' : 'Internal Server Error'}");
	}
	
	$response	= $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);
	
	$response->body($out);

});

$app->post('/customer/:customerid/client', function($customerid) use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$facade = new \RESTAPI\ClientsFacade();
	try {
		$out = $facade->addClient($macompanyid, $customerid, $app->request());
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	} catch (\Exception $e) { // I probably should ask Slim to handle all uncatched Exception
		error_log($e->getMessage());
		$app->halt(500, "{'error' : 'Internal Server Error'}");
	}

	$response	= $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$response->body(json_encode($out));

});

/* **** Below are client company actions **** */

$app->get('/customer/:customerid', function($customerid) use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$facade = new \RESTAPI\ClientCompaniesFacade();
	try {
		$out = $facade->getClientCompany($macompanyid, $customerid);
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}

	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$response->body($out);

});

$app->get('/customer', function () use ($app) {
	
	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');
	
	$facade = new \RESTAPI\ClientCompaniesFacade();
	try {
		$out = $facade->findClientCompany($macompanyid, $app->request());
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}
	
	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);
	
	$response->body($out);
	
});

$app->put('/customer/:customerid', function($customerid) use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$facade = new \RESTAPI\ClientCompaniesFacade();

	$response	= $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$response->body(json_encode($facade->updateClientCompany($macompanyid, $customerid, $app->request())));

});

$app->post('/agent/:agentid/customer', function($agentid) use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$facade = new \RESTAPI\ClientCompaniesFacade();
	$response	= $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$response->body(json_encode($facade->addClientCompany($macompanyid, $agentid, $app->request())));

});

/* **** Below are agent actions **** */

$app->get('/agent/:agentid', function ($agentid) use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$facade = new \RESTAPI\AgentsFacade();
	try {
		$out = $facade->getAgent($macompanyid, $agentid);
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}

	$response	= $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$response->body(json_encode($out));

});

/* **** Below are agent company actions **** */

$app->get('/agentcompany/:agentcompanyid', function ($agentcompanyid) use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$facade = new \RESTAPI\AgentCompaniesFacade();
	try {
		$out = $facade->getAgentCompany($macompanyid, $agentcompanyid);
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}

	$response	= $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$response->body(json_encode($out));

});

/* **** Below are request from XO **** */

$app->get('/quotesku', function () use ($app) {

	$macompanyid = \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$facade = new \RESTAPI\OrdersFacade();

	$request = $app->request();
	try {
		$out = $facade->getQuotesPerLocationWithSku($macompanyid, $request);
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}

	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$response->body(json_encode($out));
});

/* **** Below are rfq and quote actions **** */

$app->get('/rfqidsbydates', function() use ($app) {
	
	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');
	
	$facade = new \RESTAPI\RfqsFacade();
	
	try {
		$out = $facade->getRfqIdsByDates($macompanyid, $app->request());
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}
	
	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);
	
	$response->body(json_encode($out));
	
});

$app->get('/quoteidsbydates', function() use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');
	
	$facade = new \RESTAPI\RfqsFacade();
	
	try {
		$out = $facade->getQuoteIdsByDates($macompanyid, $app->request());
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}
	
	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);
	
	$response->body(json_encode($out));

});

$app->get('/locquote/:quotelocid', function($quotelocid) use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');
	
	$facade = new \RESTAPI\RfqsFacade();
	
	try {
		$out = $facade->getQuotebyLocId($macompanyid, $quotelocid);
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	} catch (\Exception $e) {
		$app->halt(500, "{'error' : 'Unknown.'}");
	}
	
	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);
	
	$response->body(json_encode($out));

});

$app->get('/quote/:quoteid', function($quoteid) use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');
	
	$facade = new \RESTAPI\RfqsFacade();
	
	try {
		$out = $facade->getQuotebyId($macompanyid, $quoteid);
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	}
	
	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);
	
	$response->body(json_encode($out));
	
});

$app->get('/locquoteskus/:quotelocid', function($quotelocid) use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');
	
	$facade = new \RESTAPI\RfqsFacade();
	
	try {
		$out = $facade->getQuoteSkusbyLocId($macompanyid, $quotelocid);
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	} catch (\Exception $e) {
		error_log($e->getMessage());
		$app->halt(500, "{'error' : 'Unknown.'}");
	}
	
	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);
	
	$response->body(json_encode($out));

});

$app->get('/locrequirement/:locreqsqid', function($locreqsqid) use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$facade = new \RESTAPI\RfqsFacade();

	try {
		$out = $facade->getLocationRequirementById($macompanyid, $locreqsqid);
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	} catch (\Exception $e) {
		$app->halt(500, "{'error' : 'Unknown.'}");
	}
	
	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);
	
	$response->body(json_encode($out));

});

$app->get('/requirement/:reqsqid', function($reqsqid) use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$facade = new \RESTAPI\RfqsFacade();

	try {
		$out = $facade->getRequirementById($macompanyid, $reqsqid);
	} catch (\RESTAPI\RESTApiException $e) {
		$app->halt($e->getCode(), "{'error' : '{$e->getMessage()}'}");
	} catch (\Exception $e) {
		$app->halt(500, "{'error' : 'Unknown.'}");
	}

	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);

	$response->body(json_encode($out));

});

/* Allows for the retrieveal of API errors in RFQs */
$app->get('/rfqerrors', function() use ($app) {

	$macompanyid	= \Slim\Environment::getInstance()->offsetGet(APPLICATION_PREFIX . 'macompanyid');

	$request = $app->request();
	$startDate = $request->params('startDate');
	$endDate   = $request->params('endDate');

	require_once DOC_ROOT . '/inc/classes/RFQ/RFQErrors.php';
	require_once DOC_ROOT . '/inc/classes/IQSQWrapper/XLog_Category.php';

	try {
		$out = RFQErrors::getRfqErrors($macompanyid, $startDate, $endDate);
	} catch (\Exception $e) {
		$app->halt($e->getCode(), '{"error" : "'.$e->getMessage().'"}');
	}

	$response = $app->response();
	$response['Content-Type'] = 'application/json';
	$response->status(200);
	
	$response->body(json_encode($out));
	
});

$app->run();
